<?php

namespace Drupal\entity_merge;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Context class used when merging fields.
 *
 * @package Drupal\entity_merge
 */
class FieldMergeContext {

  /**
   * The merge context for the request.
   *
   * @var \Drupal\entity_merge\MergeContext
   */
  private $parentContext;

  /**
   * Name of field being merged.
   *
   * @var string
   */
  private $fieldId;

  /**
   * Primary Field being merged.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  private $primaryField;

  /**
   * Secondary Field being merged.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  private $secondaryField;

  /**
   * Errors.
   *
   * @var array
   */
  private $errors = [];

  /**
   * FieldMergeContext constructor.
   *
   * @param \Drupal\entity_merge\MergeContext $parent_context
   *   Merge context.
   * @param string $field_id
   *   Field ID.
   * @param \Drupal\Core\Field\FieldItemListInterface $primaryField
   *   Primary field being merged.
   * @param \Drupal\Core\Field\FieldItemListInterface $secondaryField
   *   Secondary field being merged.
   */
  public function __construct(MergeContext $parent_context, $field_id, FieldItemListInterface $primaryField, FieldItemListInterface $secondaryField) {
    $this->parentContext = $parent_context;
    $this->fieldId = $field_id;
    $this->primaryField = $primaryField;
    $this->secondaryField = $secondaryField;
  }

  /**
   * Gets the parent merge context.
   *
   * @return \Drupal\entity_merge\MergeContext
   *   The parent merge context.
   */
  public function getParentContext(): MergeContext {
    return $this->parentContext;
  }

  /**
   * Gets the current field ID.
   *
   * @return string
   *   Field ID.
   */
  public function getFieldId(): string {
    return $this->fieldId;
  }

  /**
   * Gets the primary field item list.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list.
   */
  public function getPrimaryField(): FieldItemListInterface {
    return $this->primaryField;
  }

  /**
   * Gets the secondary field item list.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list.
   */
  public function getSecondaryField(): FieldItemListInterface {
    return $this->secondaryField;
  }

  /**
   * Adds progress message to the log.
   *
   * @param string $message
   *   The message.
   * @param mixed|null $primary_value
   *   Primary entity's field value.
   * @param mixed|null $secondary_value
   *   Secondary entity's field value.
   */
  public function addProgress($message, $primary_value = NULL, $secondary_value = NULL) {
    $this->parentContext->addProgress($message, $this->fieldId, $primary_value, $secondary_value);
  }

  /**
   * Adds an error message to the log.
   *
   * @param string $message
   *   The message.
   * @param mixed|null $primary_value
   *   Primary entity's field value.
   * @param mixed|null $secondary_value
   *   Secondary entity's field value.
   */
  public function addError($message, $primary_value = NULL, $secondary_value = NULL) {
    $this->parentContext->addError($message, $this->fieldId, $primary_value, $secondary_value);
  }

}
