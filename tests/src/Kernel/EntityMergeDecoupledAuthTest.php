<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\profile\Entity\Profile;

/**
 * Entity Merge tests.
 *
 * @group entity_merge
 */
class EntityMergeDecoupledAuthTest extends EntityMergeTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_merge',
    'user',
    'system',
    'options',
    'address',
    'datetime',
    'field',
    'taxonomy',
    'text',
    'profile',
    'node',
    'entity_merge_test',
    'decoupled_auth',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('profile');
    $this->installEntitySchema('entity_merge_request');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');

    $this->installConfig('node');
    $this->installConfig('entity_merge_test');
    $this->installConfig('user');
    $this->installConfig('decoupled_auth');
    $this->installSchema('system', 'sequences');

    $this->user1 = $this->createUser([], 'user1');
    $this->user2 = $this->createUser([], 'user2');
  }

  /**
   * Checks that username/password/email are moved over.
   *
   * Also checks that the duplicate is disabled.
   */
  public function testHandlesLoginCredentials() {
    $this->user1
      ->set('mail', NULL)
      ->set('pass', NULL)
      ->set('name', NULL);

    $this->user1->save();

    $this->runMerge();

    // User1 should end up with user2's credentials and be active.
    $this->assertEquals('user2@example.com', $this->user1->getEmail());
    $this->assertEquals('user2', $this->user1->getAccountName());
    $this->assertNotNull($this->user1->getPassword());
    $this->assertEquals('1', $this->user1->status->value);

    // User2 should have had its credentials removed and be blocked.
    $this->assertEquals('user2@example.com', $this->user2->getEmail());
    $this->assertNull($this->user2->get('name')->value);
    $this->assertNull($this->user2->getPassword());
    $this->assertEquals('0', $this->user2->status->value);
  }

  /**
   * Tests that a sub-merge runs for attached profiles.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRunsSubMergeForProfile() {
    $profile1 = Profile::create(['type' => 'crm_indiv']);
    $profile1->setOwner($this->user1);
    $profile1->save();

    $profile2 = Profile::create([
      'type' => 'crm_indiv',
      'crm_dob' => '1987-04-19',
    ]);
    $profile2->setOwner($this->user2);
    $profile2->save();

    $this->runMerge();

    $this->assertEquals('1987-04-19', $this->user1->profile_crm_indiv->entity->crm_dob->value);
  }

  /**
   * Checks that a profile can be moved from secondary to primary.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testMovesProfileAcrossWhenPrimaryDoesntHaveOne() {

    $profile2 = Profile::create([
      'type' => 'crm_indiv',
      'crm_dob' => '1987-04-19',
    ]);
    $profile2->setOwner($this->user2);
    $profile2->save();

    $this->runMerge();

    $profile2 = Profile::load($profile2->id());

    // Both sides of the reference should point to the
    // user the profile was moved to.
    $this->assertEquals($profile2->id(), $this->user1->profile_crm_indiv->entity->id());
    $this->assertEquals($this->user1->id(), $profile2->getOwnerId());
  }

  /**
   * Test that sub merge handles nested fields.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRunsSubMergeForAddressOnProfile() {
    $profile1 = Profile::create(['type' => 'crm_indiv']);
    $profile1->setOwner($this->user1);
    $profile1->save();

    $profile2 = Profile::create([
      'type' => 'crm_indiv',
      'crm_address' => [
        'country_code' => 'UK',
        'address_line1' => 'line1',
      ],
    ]);
    $profile2->setOwner($this->user2);
    $profile2->save();

    $this->runMerge();
    $this->assertEquals('line1', $this->user1->profile_crm_indiv->entity->get('crm_address')
      ->getValue()[0]['address_line1']);
  }

  /**
   * Tests NoErrorWhenProfileMissingFromSecondary.
   */
  public function testNoErrorDisplayingFieldsWhenProfileMissingFromSecondary() {
    $profile1 = Profile::create(['type' => 'crm_indiv']);
    $profile1->setOwner($this->user1);
    $profile1->save();

    $merger = $this->getMerger();
    $merger->displayFields('user', $this->user1, $this->user2);
  }

  /**
   * Tests NoErrorWhenProfileMissingFromPrimary.
   */
  public function testNoErrorMergingFieldsWhenProfileMissingFromPrimary() {
    $profile1 = Profile::create(['type' => 'crm_indiv']);
    $profile1->setOwner($this->user2);
    $profile1->save();

    $merger = $this->getMerger();
    $merger->displayFields('user', $this->user1, $this->user2);
  }

}
