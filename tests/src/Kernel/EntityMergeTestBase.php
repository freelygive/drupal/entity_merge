<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\user\Entity\User;

/**
 * Base class for testing entity merge.
 *
 * @package Drupal\Tests\entity_merge\Kernel
 */
abstract class EntityMergeTestBase extends KernelTestBase {

  /**
   * Primary user to merge.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user1;

  /**
   * Second user to merge.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user2;

  /**
   * Gets the merger.
   *
   * @return \Drupal\entity_merge\Merger
   *   The merger.
   *
   * @throws \Exception
   */
  protected function getMerger() {
    return $this->container->get('entity_merge.merger');
  }

  /**
   * Runs the merge.
   *
   * @throws \Exception
   */
  protected function runMerge($should_succeed = TRUE) {
    $request = $this->createMergeRequest($this->user1, $this->user2);
    $merger = $this->getMerger();
    $result = $merger->run($request);
    $this->user1 = User::load($this->user1->id());
    $this->user2 = User::load($this->user2->id());

    if ($should_succeed && !$result) {
      print_r($request->log->getValue());
    }

    $this->assertEquals($should_succeed, $result);
  }

  /**
   * Creates a merge request for two entities.
   *
   * @param \Drupal\user\Entity\User $user1
   *   Primary.
   * @param \Drupal\user\Entity\User $user2
   *   Secondary.
   *
   * @return \Drupal\entity_merge\Entity\MergeRequest
   *   The merge request
   */
  protected function createMergeRequest(User $user1, User $user2) {
    $request = MergeRequest::create([
      'primary_id' => $user1->id(),
      'secondary_id' => $user2->id(),
      'target_type' => 'user',
      'status' => MergeRequest::STATUS_NOT_STARTED,
    ]);
    return $request;
  }

}
