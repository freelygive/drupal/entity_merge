<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\Role;

/**
 * Tests merging organisations with group members.
 *
 * @package Drupal\Tests\entity_merge\Kernel
 */
class GroupMergeTest extends EntityMergeTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_merge',
    'user',
    'system',
    'options',
    'address',
    'datetime',
    'field',
    'taxonomy',
    'text',
    'node',
    'profile',
    'decoupled_auth',
    'contacts',
    'crm_tools',
    'group',
    'flexible_permissions',
    'contacts_group',
    'ctools',
    'image',
    'file',
    'search_api',
    'search_api_db',
    'facets',
    'name',
    'entity_merge_test',
    'views',
    'ctools_views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_merge_request');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');
    $this->installEntitySchema('profile');
    $this->installEntitySchema('group');
    $this->installEntitySchema('group_content');
    $this->installEntitySchema('file');
    $this->installEntitySchema('search_api_task');

    $this->installConfig('node');
    $this->installConfig('entity_merge_test');
    $this->installConfig('user');
    $this->installConfig('search_api');
    $this->installConfig('facets');
    $this->installConfig('image');
    $this->installConfig('name');
    $this->installConfig('views');
    $this->installConfig('profile');
    $this->installConfig('contacts');
    $this->installConfig('crm_tools');
    $this->installConfig('flexible_permissions');
    $this->installConfig('group');

    Role::create(['id' => 'administrator', 'label' => 'Administrator'])->save();
    Role::create(['id' => 'crm_manager', 'label' => 'CRM Manager'])->save();

    $this->installConfig('contacts_group');

    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');

    $this->user1 = $this->createUser([], 'org1');
    $this->user2 = $this->createUser([], 'org2');

    $this->user1->addRole('crm_org');
    $this->user2->addRole('crm_org');
    $this->user1->save();
    $this->user2->save();
  }

  /**
   * Tests MergesGroups.
   */
  public function testMergesGroups() {
    // Create 2 groups with members. Each group assigned to an organisation.
    /** @var \Drupal\group\Entity\Group $group1 */
    /** @var \Drupal\group\Entity\Group $group2 */
    $group1 = Group::create([
      'type' => 'contacts_org',
      'contacts_org' => $this->user1->id(),
    ]);
    $group1->save();

    $group2 = Group::create([
      'type' => 'contacts_org',
      'contacts_org' => $this->user2->id(),
    ]);
    $group2->save();

    $member1 = $this->createUser([], 'member1');
    $group1->addMember($member1);
    $group1->save();

    $member2 = $this->createUser([], 'member2');
    $group2->addMember($member2);
    $group2->save();

    $this->runMerge();

    // After merging the 2 organisations, group2 should have 2 members and
    // group1 should have 0 members.
    $group1 = Group::load($group1->id());
    $group2 = Group::load($group2->id());

    $this->assertEquals(2, count($group1->getRelationships()));
    $this->assertEquals(0, count($group2->getRelationships()));

    $members = $group1->getMembers();

    $this->assertEquals($member1->id(), $members[0]->getUser()->id());
    $this->assertEquals($member2->id(), $members[1]->getUser()->id());
  }

}
