<?php

namespace Drupal\entity_merge\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Default field formatter for merge logs.
 *
 * @FieldFormatter(
 *   id = "entity_merge_log_entry",
 *   label = @Translation("Merge Log"),
 *   field_types = {
 *     "entity_merge_log_entry",
 *   },
 * )
 */
class MergeRequestLogFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $output = [];
    $rows = [];

    foreach ($items->getValue() as $log) {
      $rows[] = [
        $log['message'],
        $log['field'],
        $log['primary_value'],
        $log['secondary_value'],
      ];
    }

    // Must be keyed as 0.
    $output[0] = [
      '#type' => 'table',
      '#empty' => $this->t('The log is empty'),
      '#header' => [
        $this->t('Message'),
        $this->t('Field'),
        $this->t('Value 1'),
        $this->t('Value 2'),
      ],
      '#rows' => $rows,
    ];

    return $output;
  }

}
