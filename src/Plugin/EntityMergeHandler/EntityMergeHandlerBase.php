<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\MergeContext;
use Drupal\entity_merge\Plugin\EntityMergeHandlerInterface;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity merge handlers.
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
abstract class EntityMergeHandlerBase implements EntityMergeHandlerInterface, ContainerFactoryPluginInterface {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function displayFields(MergeContext $context) {
    // By default, return all fields on the entity except for the entity keys.
    // Return them all in a single group.
    // Subclasses can override this behaviour.
    $keys = $this->getDisplayFieldsToSkip($context);

    // Key is the field group (blank for default).
    $fields = [
      '' => [],
    ];

    foreach ($context->getPrimary() as $field_id => $field) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $field */

      if (in_array($field_id, $keys)) {
        continue;
      }

      $field_handlers = $this->getFieldHandlers($context, $field_id, $field->getFieldDefinition());
      $secondary_field = $context->getSecondary()->get($field_id);
      $field_context = new FieldDisplayContext($context, $field_id, $field, $secondary_field);

      foreach ($field_handlers as $field_handler) {
        if ($field_handler->displayField($field_context) == FieldMergeHandlerInterface::HANDLED) {
          $fields = array_merge_recursive($fields, $field_context->getFieldDisplayInfo());
          break;
        }
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(MergeContext $context) {
    // If they're different bundles, just give up now.
    if ($context->getPrimary()->bundle() != $context->getSecondary()->bundle()) {
      $context->addError("Cannot merge entities with different bundles. Primary: {$context->getPrimary()->bundle()} Secondary: {$context->getSecondary()->bundle()}");
      return;
    }
    $this->mergeFields($context);
    $this->postMerge($context);
    $this->save($context);
  }

  /**
   * Merges all fields on the target entities.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Merge context representing the merge.
   */
  protected function mergeFields(MergeContext $context) {
    $fields_to_skip = $this->getFieldsToSkip($context);

    foreach ($context->getPrimary() as $field_id => $primary_field) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $primary_field */
      // Skip fields that are defined as entity keys.
      if (in_array($field_id, $fields_to_skip)) {
        continue;
      }

      $context->addProgress('Processing field', $field_id);

      try {
        $primary_value = $primary_field->getValue();
        $secondary_field = $context->getSecondary()->{$field_id};
        $secondary_value = $secondary_field->getValue();
        $field_definition = $primary_field->getFieldDefinition();

        $field_handlers = $this->getFieldHandlers($context, $field_id, $field_definition);

        $field_handled = FALSE;

        foreach ($field_handlers as $handler) {
          // Give each handler a chance to process the field.
          // Once one has successfully processed the field, prevent any more
          // from doing additional processing.
          $field_context = new FieldMergeContext($context, $field_id, $primary_field, $secondary_field);

          $field_result = $handler->performMerge($field_context);

          // The field has been handled. Move on to next field.
          // If not, then allow the next handler for this field to try.
          if ($field_result == FieldMergeHandlerInterface::HANDLED) {
            $field_handled = TRUE;
            break;
          }
        }

        // No handlers successfully handled the field.
        // Record an error.
        if (!$field_handled) {
          $context->addError('Unable to handle field', $field_id);
        }
      }
      catch (\Exception $ex) {
        $context->addError($ex->getMessage(), $field_id);
      }
    }
  }

  /**
   * Perform any post merge operations prior to saving.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Merge context representing the merge.
   */
  protected function postMerge(MergeContext $context) {
    // Allow extending classes to implement specific logic.
  }

  /**
   * Gets the field handler to use.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   The merge context.
   * @param string $field_id
   *   Field ID/name.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface[]
   *   Array of field merge handlers
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getFieldHandlers(MergeContext $context, $field_id, FieldDefinitionInterface $field_definition) {
    // Find the handler for this field.
    // Is there a handler this entity type and field name?
    $field_handlers = $context->findFieldHandlersByFieldName($context->getPrimary()->getEntityTypeId(), $field_id);

    if (!count($field_handlers)) {
      // Instead try by entity type and field type.
      $field_handlers_by_field_type_and_entity_type = $context->findFieldHandlersByFieldType($field_definition->getType(), $context->getPrimary()->getEntityTypeId());

      // Also try by just field type (not entity type).
      $field_handlers_by_field_type = $context->findFieldHandlersByFieldType($field_definition->getType());
      $field_handlers = array_merge($field_handlers_by_field_type_and_entity_type, $field_handlers_by_field_type);
    }

    // Add default field handler as the last one if nothing else can handle it.
    $field_handlers[] = $context->getDefaultFieldHandler();
    return $field_handlers;
  }

  /**
   * Saves entities post-merge.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Context representing the merge.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function save(MergeContext $context) {
    if (!$context->hasErrors()) {
      $context->getSecondary()->save();
      $context->getPrimary()->save();
    }
  }

  /**
   * Fields that should not be processed.
   *
   * By default, the entity keys.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Merge context.
   *
   * @return array
   *   Array of fields to skip
   */
  protected function getFieldsToSkip(MergeContext $context): array {
    $keys = $context->getPrimary()->getEntityType()->getKeys();
    // Allow merging of label.
    unset($keys['label']);
    return $keys;
  }

  /**
   * Fields that should not be displayed on the verify form.
   *
   * By default, the entity keys.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Merge context.
   *
   * @return array
   *   Array of fields to skip
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    $keys = $context->getPrimary()->getEntityType()->getKeys();
    unset($keys['label']);
    return $keys;
  }

  /**
   * Re-points any entity references to the secondary entity to the primary.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Context.
   */
  public function repointEntityReferences(MergeContext $context) {
    $entity_reference_fields = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');
    $fields_to_skip = $this->getFieldsToSkipRepointingReferences();

    foreach ($entity_reference_fields as $entity_type_id => $fields) {
      if ($entity_type_id == 'entity_merge_request') {
        // Always skip references back to the Merge Request.
        continue;
      }

      /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type */
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      $fields_to_update = [];

      $bundle_field = $entity_type->getKey('bundle');
      $base_fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);

      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $query = $storage->getQuery();
      $query->accessCheck(FALSE);
      $or = $query->orConditionGroup();
      $query->condition($or);

      $revision_metadata = $entity_type->getRevisionMetadataKeys();

      foreach ($fields as $field_id => $field) {
        // Skip field if excluded.
        if (isset($fields_to_skip[$entity_type_id]) && in_array($field_id, $fields_to_skip[$entity_type_id])) {
          continue;
        }

        // @todo Handle revisions and revision metadata.
        if (in_array($field_id, $revision_metadata)) {
          continue;
        }

        if ($bundle_field && !empty($field['bundles'])) {
          // Bundle field.
          $bundles = [];

          foreach ($field['bundles'] as $bundle) {
            $field_def = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle)[$field_id];
            // Computed fields do not require repointing so skip them.
            if ($field_def->isComputed()) {
              continue;
            }
            if ($field_def->getSetting('target_type') === $context->getPrimary()->getEntityTypeId()) {
              $bundles[] = $bundle;
              $fields_to_update[$bundle][] = $field_id;
            }
          }

          if ($bundles) {
            $and = $query->andConditionGroup();
            $and->condition($field_id, $context->getSecondary()->id());
            $and->condition($bundle_field, $bundles, 'IN');
            $or->condition($and);
          }
        }
        elseif (isset($base_fields[$field_id])) {
          /** @var \Drupal\Core\Field\BaseFieldDefinition $field_def */
          $field_def = $base_fields[$field_id];
          // Computed fields do not require repointing so skip them.
          if ($field_def->isComputed()) {
            continue;
          }
          $target_type = $field_def->getSetting('target_type');
          if ($target_type === $context->getPrimary()->getEntityTypeId()) {
            $fields_to_update[''][] = $field_id;
            $or->condition($field_id, $context->getSecondary()->id());
          }
        }
      }

      if ($or->count()) {
        $ids = $query->execute();
        $entities = $storage->loadMultiple($ids);

        foreach ($entities as $entity) {
          foreach ($fields_to_update as $bundle => $fields) {
            if ($bundle === '' || $bundle === $entity->bundle()) {
              foreach ($fields as $field_id) {
                $context->addProgress('Re-pointing entity reference', "$entity_type_id:$field_id ({$entity->id()})", $context->getPrimary()->id(), $context->getSecondary()->id());
                $entity->set($field_id, $context->getPrimary()->id());
              }
            }
          }
          $entity->save();
        }
      }
    }
  }

  /**
   * Gets the entity type IDs to skip when re-pointing references.
   *
   * @return array
   *   Nested array. Key is the entity type ID,
   *   value is an array of field names.
   */
  protected function getFieldsToSkipRepointingReferences() : array {
    return [];
  }

}
