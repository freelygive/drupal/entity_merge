<?php

namespace Drupal\entity_merge\Plugin;

use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;

/**
 * Defines a plugin used to merge a field.
 *
 * @package Drupal\entity_merge\Plugin
 */
interface FieldMergeHandlerInterface {

  /**
   * Indicates a field handler ran for a specified field.
   */
  const HANDLED = 1;

  /**
   * Indicates a field handler did not run for a specified field.
   */
  const NOT_HANDLED = 0;

  /**
   * Merges a field.
   *
   * @param \Drupal\entity_merge\FieldMergeContext $context
   *   Context representing the merge.
   *
   * @return int
   *   Whether or not the handler handled the field.
   */
  public function performMerge(FieldMergeContext $context);

  /**
   * Builds display metadata for the field, used on the merge preview screen.
   *
   * @param \Drupal\entity_merge\FieldDisplayContext $context
   *   Context representing the merge.
   *
   * @return int
   *   Whether or not the handler handled the field.
   */
  public function displayField(FieldDisplayContext $context);

}
