<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\entity_merge\MergeContext;

/**
 * Default entity merge handler.
 *
 * @EntityMergeHandler(
 *   id = "user",
 * )
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
class UserEntityMergeHandler extends EntityMergeHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function displayFields(MergeContext $context) {
    $fields = parent::displayFields($context);

    // Display simplenews subscribers, if enabled.
    if ($this->entityTypeManager->hasDefinition('simplenews_subscriber')) {
      $this->displaySubscribers($context, $fields);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function postMerge(MergeContext $context) {
    // Make sure duplicate user is disabled.
    $context->getSecondary()->set('status', FALSE);

    // Merge simplenews subscribers, if enabled.
    if ($this->entityTypeManager->hasDefinition('simplenews_subscriber')) {
      $this->mergeSubscribers($context);
    }

    parent::postMerge($context);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkip(MergeContext $context): array {
    $skip = parent::getFieldsToSkip($context);

    // Preserve enable/disabled state.
    $skip[] = 'status';

    return $skip;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    $skip = array_merge(parent::getDisplayFieldsToSkip($context), [
      'preferred_langcode',
      'preferred_admin_langcode',
      'init',
      'pass',
    ]);

    return $skip;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkipRepointingReferences(): array {
    $fields = parent::getFieldsToSkipRepointingReferences();
    $fields['simplenews_subscriber'][] = 'uid';
    return $fields;
  }

  /**
   * Display simplenews subscribers for these users.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   The merge context.
   * @param array $fields
   *   The fields array to add to.
   */
  protected function displaySubscribers(MergeContext $context, array &$fields) {
    $storage = $this->entityTypeManager
      ->getStorage('simplenews_subscriber');

    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $context->getPrimary()->id())
      ->sort('status', 'DESC');
    /** @var \Drupal\simplenews\SubscriberInterface[] $primary_subscribers */
    $primary_subscribers = $storage->loadMultiple($query->execute());
    $displayed_emails = [];
    foreach ($primary_subscribers as $primary_subscriber) {
      $displayed_emails[] = $primary_subscriber->getMail();
      $outer_group = "Subscriber {$primary_subscriber->getMail()} [{$primary_subscriber->id()}]";

      $query = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('uid', $context->getSecondary()->id())
        ->condition('mail', $primary_subscriber->getMail())
        ->sort('status', 'DESC');
      /** @var \Drupal\simplenews\SubscriberInterface[] $secondary_subscribers */
      $secondary_subscribers = $storage->loadMultiple($query->execute());
      if ($secondary_subscribers) {
        foreach ($secondary_subscribers as $secondary_subscriber) {
          $new_context = $context->createChildContext('simplenews_subscriber', $primary_subscriber, $secondary_subscriber);
          $handler = $new_context->getEntityPluginManager()
            ->createInstance('simplenews_subscriber');
          $result = $handler->displayFields($new_context);
          foreach ($result as $group => $field_group) {
            foreach ($field_group as $field_id => $field_display) {
              $group_name = $group ? "{$outer_group}: {$group}" : $outer_group;
              $fields[$group_name][$field_id] = $field_display;
            }
          }
        }
      }
      else {
        $fields['']["subscriber.{$primary_subscriber->id()}"] = [
          'comparison' => TRUE,
          'label' => $outer_group,
          'primary' => new PluralTranslatableMarkup(count($primary_subscriber->getSubscribedNewsletterIds()), '1 subscription', '@count subscriptions'),
          'secondary' => NULL,
        ];
      }
    }

    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $context->getSecondary()->id())
      ->sort('status', 'DESC');
    if (!empty($displayed_emails)) {
      $query->condition('mail', $displayed_emails, 'NOT IN');
    }
    /** @var \Drupal\simplenews\SubscriberInterface[] $secondary_subscribers */
    $secondary_subscribers = $storage->loadMultiple($query->execute());
    foreach ($secondary_subscribers as $subscriber) {
      $fields['']["subscriber.{$subscriber->id()}"] = [
        'comparison' => TRUE,
        'label' => "Subscriber {$subscriber->getMail()} [{$subscriber->id()}]",
        'primary' => NULL,
        'secondary' => new PluralTranslatableMarkup(count($subscriber->getSubscribedNewsletterIds()), '1 subscription', '@count subscriptions'),
      ];
    }
  }

  /**
   * Merge simplenews subscribers for these users.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   The merge context.
   */
  protected function mergeSubscribers(MergeContext $context) {
    // Find all subscribers, merging them if they have the same email addresses.
    $storage = $this->entityTypeManager
      ->getStorage('simplenews_subscriber');

    // Get the first subscriber for each email address for the primary user,
    // preferring active.
    // @todo Refactor if simplenews ensures uniqueness, as discussed in
    // https://www.drupal.org/project/simplenews/issues/2937251.
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $context->getPrimary()->id())
      ->sort('status', 'DESC');
    /** @var \Drupal\simplenews\SubscriberInterface[] $subscribers */
    $subscribers = $storage->loadMultiple($query->execute());
    $primary_subscribers = [];
    foreach ($subscribers as $subscriber) {
      $primary_subscribers += [
        $subscriber->getMail() => $subscriber,
      ];
    }

    // Now loop over each secondary subscriber to and merge them in.
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $context->getSecondary()->id())
      ->sort('status', 'DESC');
    $subscribers = $storage->loadMultiple($query->execute());
    foreach ($subscribers as $subscriber) {
      // If this email address doesn't exist for the primary, just move it over.
      if (!isset($primary_subscribers[$subscriber->getMail()])) {
        // Make sure we use set('uid') and not setUserId()
        // as setUserId() is only available in simplenews 1.x
        // but set('uid') can be used in 1.x, 2.x and 3.x.
        $subscriber->set('uid', $context->getPrimary()->id());
        $subscriber->save();
        $context->addProgress('Subscriber moved to primary user.', NULL, NULL, $subscriber->id());
      }
      else {
        $primary = $primary_subscribers[$subscriber->getMail()];
        $new_context = $context->createChildContext('simplenews_subscriber', $primary, $subscriber, "subscriber:{$primary->id()}");
        $handler = $new_context->getEntityPluginManager()
          ->createInstance('simplenews_subscriber');
        $handler->performMerge($new_context);
      }
    }
  }

}
