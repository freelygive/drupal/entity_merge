<?php

/**
 * @file
 * Contains entity_merge_request.page.inc.
 *
 * Page callback for Merge request entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Merge request templates.
 *
 * Default template: entity_merge_request.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_entity_merge_request(array &$variables) {
  // Fetch MergeRequest Entity Object.
  $entity_merge_request = $variables['elements']['#entity_merge_request'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
