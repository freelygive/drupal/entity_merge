<?php

namespace Drupal\entity_merge\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_merge\Merger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for creating new merge requests.
 *
 * @ingroup entity_merge
 */
class MergeRequestForm extends FormBase {

  /**
   * Merge coordinator.
   *
   * @var \Drupal\entity_merge\Merger
   */
  protected $merger;

  /**
   * {@inheritdoc}
   */
  public function __construct(Merger $merger) {
    $this->merger = $merger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_merge.merger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "entity_merge_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['selection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select entities to merge'),
      '#description' => $this->t('Enter the IDs of the entities you want to merge.'),
      '#tree' => TRUE,
    ];
    $form['selection']['#collapsible'] =
    $form['selection']['#collapsed'] = $form_state->isSubmitted();

    $form['selection']['primary_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Entity Id'),
      '#description' => $this->t('Data from the secondary entity will be merged into this entity.'),
      '#required' => TRUE,
    ];

    $form['selection']['secondary_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Duplicate entity id'),
      '#description' => $this->t('Data from this entity will be merged into the primary entity.'),
      '#required' => TRUE,
    ];

    $form['selection']['actions'] = ['#type' => 'actions'];
    $form['selection']['actions']['verify'] = [
      '#type' => 'submit',
      '#name' => 'verify_merge',
      '#value' => $this->t('Verify merge'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $primary_id = $form_state->getValue('selection')['primary_id'];
    $secondary_id = $form_state->getValue('selection')['secondary_id'];
    $entity_type = $this->getRouteMatch()->getParameter('entity_type');

    $form_state->setRedirect('entity.entity_merge_request.verify', [
      'entity_type' => $entity_type,
      'primary_id' => $primary_id,
      'secondary_id' => $secondary_id,
    ]);
  }

}
