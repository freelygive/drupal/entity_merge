<?php

namespace Drupal\entity_merge;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Merge request entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class MergeRequestHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();
    $collection->add("entity.{$entity_type_id}.verify", $this->getVerifyFormRoute($entity_type));
    return $collection;
  }

  /**
   * {@inheritdoc}
   *
   * Overridden as this isn't an entity form.
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('add-form'));
    $route->setDefault('_form', '\Drupal\entity_merge\Form\MergeRequestForm');
    // Use the bundle feature of the entity create access checks to check our
    // merge entity type.
    $route->setRequirement('_entity_create_access', $entity_type->id() . ':{entity_type}');
    return $route;
  }

  /**
   * Gets the merge verify route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route
   *   The generated route.
   */
  protected function getVerifyFormRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('verify'));
    $route->setDefault('_form', '\Drupal\entity_merge\Form\MergeVerifyForm');
    $route->setRequirement('_entity_create_access', $entity_type->id());
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection') && $entity_type->hasListBuilderClass()) {
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
      $label = $entity_type->getCollectionLabel();

      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->addDefaults([
          '_entity_list' => $entity_type->id(),
          '_title' => $label->getUntranslatedString(),
          '_title_arguments' => $label->getArguments(),
          '_title_context' => $label->getOption('context'),
        ])
        ->setRequirement('_permission', 'view all merge requests');

      return $route;
    }
  }

}
