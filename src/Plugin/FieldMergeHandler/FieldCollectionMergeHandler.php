<?php

namespace Drupal\entity_merge\Plugin\FieldMergeHandler;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles merging profile fields.
 *
 * @FieldMergeHandler(
 *   id = "merge_field_handler_field_collection",
 *   handles_field_type = "field_collection"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class FieldCollectionMergeHandler implements FieldMergeHandlerInterface, ContainerFactoryPluginInterface {

  /**
   * The field collection entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Construct the field collection merge handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('field_collection_item');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context
      ->getPrimaryField()
      ->getFieldDefinition();
    $cardinality = $field_definition
      ->getFieldStorageDefinition()
      ->getCardinality();
    $primary = $context->getPrimaryField();
    $secondary = $context->getSecondaryField();

    // If there are no secondary values, there's nothing to do.
    if (count($secondary) == 0) {
      return FieldMergeHandlerInterface::HANDLED;
    }

    // If multi-cardinality or only the secondary has a value, we'll copy items
    // from the second onto the first.
    if ($cardinality != 1 || count($primary) == 0) {
      foreach ($secondary as $item) {
        /** @var \Drupal\field_collection\Entity\FieldCollectionItem|null $field_collection_item */
        if ($field_collection_item = $item->getFieldCollectionItem()) {
          /** @var \Drupal\field_collection\Entity\FieldCollectionItem $duplicate */
          $duplicate = $field_collection_item->createDuplicate();
          $duplicate->setHostEntity($primary->getEntity(), FALSE);
          $duplicate->save();
        }
      }

      return FieldMergeHandlerInterface::HANDLED;
    }

    /** @var \Drupal\field_collection\Entity\FieldCollectionItem $new_primary */
    /** @var \Drupal\field_collection\Entity\FieldCollectionItem $new_secondary */
    $new_primary = $primary->first()->getFieldCollectionItem();
    $new_secondary = $secondary->first()->getFieldCollectionItem();

    // If we can't find the host ID for the field collection item,
    // abort the merge.
    if ($new_primary->getHostId() === FALSE) {
      $context->addError('Missing host for field collection item ' . $new_primary->id());
    }
    elseif ($new_secondary->getHostId() === FALSE) {
      $context->addError('Missing host for field collection item ' . $new_secondary->id());
    }
    else {
      // Perform the sub-merge.
      $new_context = $context->getParentContext()->createChildContext('field_collection_item', $new_primary, $new_secondary, $field_definition->getName());
      $handler = $new_context->getEntityPluginManager()->createInstance('field_collection_item');
      $handler->performMerge($new_context);
    }

    return FieldMergeHandlerInterface::HANDLED;
  }

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {
    $field_definition = $context
      ->getPrimaryField()
      ->getFieldDefinition();
    $cardinality = $field_definition
      ->getFieldStorageDefinition()
      ->getCardinality();

    // If multi-cardinality, we'll merge the values together.
    $primary_count = count($context->getPrimaryField());
    $secondary_count = count($context->getSecondaryField());
    if ($cardinality != 1) {
      $context->setFieldDisplayInfo('', $context->getFieldId(), [
        // Successful merge if there is enough space for all values.
        'comparison' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED || $cardinality >= ($primary_count + $secondary_count),
        'label' => $field_definition->getLabel(),
        'primary' => new PluralTranslatableMarkup($primary_count, '1 item', '@count items'),
        'secondary' => new PluralTranslatableMarkup($secondary_count, '1 item', '@count items'),
      ]);
    }
    // Otherwise we do a deep merge, so step into another level.
    elseif ($primary_count || $secondary_count) {
      /** @var \Drupal\field_collection\FieldCollectionItemInterface $primary_child */
      $primary_child = $context->getPrimary()->get($context->getFieldId())->entity;
      if (!$primary_child) {
        $primary_child = $this->storage->create([
          'field_name' => $context->getFieldId(),
          'host_type' => $field_definition->getTargetEntityTypeId(),
        ]);
      }

      /** @var \Drupal\field_collection\FieldCollectionItemInterface $secondary_child */
      $secondary_child = $context->getSecondary()->get($context->getFieldId())->entity;
      if (!$secondary_child) {
        $secondary_child = $this->storage->create([
          'field_name' => $context->getFieldId(),
          'host_type' => $field_definition->getTargetEntityTypeId(),
        ]);
      }

      $new_context = $context->getParentContext()->createChildContext('field_collection_item', $primary_child, $secondary_child);
      $handler = $new_context->getEntityPluginManager()->createInstance('field_collection_item');
      $result = $handler->displayFields($new_context);
      $outer_group = $field_definition->getLabel();

      foreach ($result as $group => $field_group) {
        $group_name = $group ? "{$outer_group}: {$group}" : $outer_group;
        foreach ($field_group as $field_id => $field_display) {
          $context->setFieldDisplayInfo($group_name, $field_id, $field_display);
        }
      }
    }

    return FieldMergeHandlerInterface::HANDLED;
  }

}
