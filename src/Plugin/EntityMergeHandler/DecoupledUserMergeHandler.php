<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\entity_merge\MergeContext;

/**
 * Merge handler for decoupled auth users.
 *
 * The default user merge handler is swapped out by this one if decoupled_auth
 * is enabled in entity_merge_entity_merge_handler_info_alter.
 *
 * @package Drupal\entity_merge_decoupled_auth\Plugin\EntityMergeHandler
 */
class DecoupledUserMergeHandler extends UserEntityMergeHandler {

  /**
   * {@inheritdoc}
   */
  public function postMerge(MergeContext $context) {
    /** @var \Drupal\decoupled_auth\Entity\DecoupledAuthUser $primary */
    /** @var \Drupal\decoupled_auth\Entity\DecoupledAuthUser $secondary */
    $primary = $context->getPrimary();
    $secondary = $context->getSecondary();

    // We can't have two users for the same account, so decouple the secondary.
    if ($primary->getAccountName() == $secondary->getAccountName()) {
      $secondary->decouple();
    }

    parent::postMerge($context);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkipRepointingReferences(): array {
    $fields_to_skip = parent::getFieldsToSkipRepointingReferences();
    $fields_to_skip['profile'][] = 'uid';
    return $fields_to_skip;
  }

}
