<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\entity_merge\MergeContext;

/**
 * Profile entity merge handler.
 *
 * @EntityMergeHandler(
 *   id = "profile",
 * )
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
class ProfileEntityMergeHandler extends EntityMergeHandlerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getFieldsToSkip($context), [
      'is_default',
      'revision_default',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getDisplayFieldsToSkip($context), [
      'is_default',
      'revision_default',
      'revision_user',
      'revision_created',
      'revision_log_message',
      'uid',
      'data',
      'created',
      'changed',
    ]);
  }

}
