<?php

namespace Drupal\entity_merge\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_merge\Plugin\EntityMergeHandler\DecoupledUserMergeHandler;

/**
 * Entity merger plugin manager.
 *
 * @package Drupal\entity_merge\Plugin
 */
class EntityMergeHandlerPluginManager extends DefaultPluginManager {

  /**
   * Constructor for EntityMergePluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntityMergeHandler', $namespaces, $module_handler, 'Drupal\entity_merge\Plugin\EntityMergeHandlerInterface', 'Drupal\entity_merge\Annotation\EntityMergeHandler');

    $this->alterInfo('entity_merge_handler_info');
    $this->setCacheBackend($cache_backend, 'entity_merge_handler_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterDefinitions(&$definitions) {
    // If decoupled_auth is enabled,
    // swap out the default merge handler for users.
    if ($this->moduleHandler->moduleExists('decoupled_auth')) {
      $definitions['user']['class'] = DecoupledUserMergeHandler::class;
    }

    parent::alterDefinitions($definitions);
  }

}
