<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Entity Merge tests.
 *
 * @group entity_merge
 */
class EntityMergeTest extends EntityMergeTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_merge',
    'user',
    'system',
    'options',
    'address',
    'datetime',
    'field',
    'taxonomy',
    'text',
    'node',
    'profile',
    'entity_merge_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_merge_request');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('node');
    $this->installEntitySchema('profile');

    $this->installConfig('node');
    $this->installConfig('entity_merge_test');
    $this->installConfig('user');
    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installSchema('user', 'users_data');

    $this->user1 = $this->createUser([], 'user1');
    $this->user2 = $this->createUser([], 'user2');
  }

  /**
   * Tests fields are not modifies when they already contain a value.
   */
  public function testDoesNotCopyValueWhenAlreadyExists() {
    $this->runMerge();
    $this->assertEquals('user1@example.com', $this->user1->getEmail());
    $this->assertEquals('user2@example.com', $this->user2->getEmail());
  }

  /**
   * Tests value is copied from secondary to primary entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCopiesField() {
    $this->user2->set('field_test_textfield', 'foo');
    $this->user2->save();

    $this->runMerge();

    // Value should be copied, but not removed from secondary.
    $this->assertEquals('foo', $this->user1->get('field_test_textfield')->value);
    $this->assertEquals('foo', $this->user2->get('field_test_textfield')->value);
  }

  /**
   * Checks entity reference is added to primary entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testHandlesEntityReference() {
    $node = Node::create(['type' => 'article', 'title' => 'foo']);
    $node->save();

    $this->user2->set('field_test_reference', $node);
    $this->user2->save();

    $this->runMerge();

    $this->assertEquals($node->id(), $this->user1->get('field_test_reference')->entity->id());
  }

  /**
   * Checks entity reference is added to primary entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testHandlesMultipleCardinalityEntityReferences() {
    $role1 = $this->createRole([], 'role1');
    $role2 = $this->createRole([], 'role2');

    $this->user1->addRole($role1);
    $this->user2->addRole($role1);
    $this->user2->addRole($role2);

    $this->user1->save();
    $this->user2->save();

    $this->runMerge();

    $user1_roles = $this->user1->getRoles();
    $user2_roles = $this->user2->getRoles();

    sort($user1_roles);
    sort($user2_roles);

    $this->assertEquals(['authenticated', $role1, $role2], $user1_roles);
    $this->assertEquals(['authenticated', $role1, $role2], $user2_roles);
  }

  /**
   * Checks entity reference is added to primary entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testHandlesTaxonomy() {
    $taxonomy = Term::create(['name' => 'foo', 'vid' => 'bar']);
    $taxonomy->save();

    $this->user2->set('field_test_taxonomy', $taxonomy);
    $this->user2->save();

    $this->runMerge();

    $this->assertEquals($taxonomy->id(), $this->user1->get('field_test_taxonomy')->entity->id());
  }

  /**
   * Tests that addresses are copied across.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCopiesAddress() {
    $this->user2->set('field_test_address', [
      'country_code' => 'UK',
      'address_line1' => 'line1',
      'address_line2' => 'line2',
    ]);

    $this->user2->save();

    $this->assertEquals(1, count($this->user2->get('field_test_address')
      ->getValue()));

    $this->runMerge();

    $this->assertEquals('line1', $this->user1->get('field_test_address')
      ->getValue()[0]['address_line1']);
    $this->assertEquals('line2', $this->user1->get('field_test_address')
      ->getValue()[0]['address_line2']);
    $this->assertEquals('UK', $this->user1->get('field_test_address')
      ->getValue()[0]['country_code']);

  }

  /**
   * Test that address merge doesn't overwrite an existing address.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDoesntCopyAddressIfHasOneAlready() {
    $this->user1->set('field_test_address', [
      'country_code' => 'UK',
      'address_line1' => 'line1',
    ]);

    $this->user2->set('field_test_address', [
      'country_code' => 'UK',
      'address_line1' => 'foo',
    ]);

    $this->user1->save();
    $this->user2->save();

    $this->assertEquals(1, count($this->user2->get('field_test_address')
      ->getValue()));

    $this->runMerge();

    $this->assertEquals('line1', $this->user1->get('field_test_address')
      ->getValue()[0]['address_line1']);
  }

  /**
   * Tests RepointsReferences.
   */
  public function testRepointsReferences() {
    $node = Node::create([
      'type' => 'article',
      'title' => 'foo',
      'uid' => $this->user2->id(),
    ]);
    $node->save();

    $this->runMerge();

    // Force reload to get new values.
    $node = Node::load($node->id());

    $this->assertEquals($this->user1->id(), $node->getOwnerId());
  }

  /**
   * Tests DeduplicatesMergedReference.
   */
  public function testDeduplicatesMergedReference() {
    $node1 = Node::create(['type' => 'article', 'title' => 'foo']);
    $node1->save();
    $node2 = Node::create(['type' => 'article', 'title' => 'bar']);
    $node2->save();
    $node3 = Node::create(['type' => 'article', 'title' => 'foo']);
    $node3->save();

    $this->user1->set('field_test_reference_multi', [$node1, $node2]);
    $this->user1->save();

    $this->user2->set('field_test_reference_multi', [$node2, $node3]);
    $this->user2->save();

    $this->runMerge();

    $field_value = $this->user1->get('field_test_reference_multi')->getValue();
    $this->assertEquals(3, count($field_value));
    $this->assertEquals(1, $field_value[0]['target_id']);
    $this->assertEquals(2, $field_value[1]['target_id']);
    $this->assertEquals(3, $field_value[2]['target_id']);
  }

  /**
   * Tests DeduplicatesTextField.
   */
  public function testDeduplicatesMergedTextField() {
    $this->user1->set('field_test_textfield_multi', ['foo', 'baz']);
    $this->user1->save();
    $this->user2->set('field_test_textfield_multi', ['foo', 'bar']);
    $this->user2->save();

    $this->runMerge();

    $field = $this->user1->get('field_test_textfield_multi');
    $this->assertEquals(3, count($field));
    $this->assertEquals('foo', $field->get(0)->value);
    $this->assertEquals('baz', $field->get(1)->value);
    $this->assertEquals('bar', $field->get(2)->value);
  }

  /**
   * Tests what happens if a user is deleted prior to the merge running.
   */
  public function testUserDeletedPriorToMergeRunning() {
    // The merge should not crash or throw an exception, and the merge should
    // complete in a Failed state, and a log message should be stored.
    $this->user1->save();
    $this->user2->save();

    $request = $this->createMergeRequest($this->user1, $this->user2);
    $request->save();

    // Delete our user.
    $this->user2->delete();

    // Force a reload of the request to mimic a real merge process.
    /** @var \Drupal\entity_merge\Entity\MergeRequest $request */
    $request = \Drupal::entityTypeManager()
      ->getStorage('entity_merge_request')
      ->loadUnchanged($request->id());

    $merger = $this->getMerger();
    $result = $merger->run($request);

    $this->assertFalse($result);
    $this->assertEquals(MergeRequest::STATUS_FAILED, $request->getStatus());
    $this->assertEquals('The secondary entity was deleted before the merge could run.', $request->get('log')->first()->message);
  }

}
