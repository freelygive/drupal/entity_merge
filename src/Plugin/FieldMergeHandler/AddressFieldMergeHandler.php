<?php

namespace Drupal\entity_merge\Plugin\FieldMergeHandler;

use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;

/**
 * Handles address fields.
 *
 * @FieldMergeHandler(
 *   id = "merge_field_handler_address",
 *   handles_field_type = "address",
 *   weight = 100
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class AddressFieldMergeHandler extends DefaultFieldMergeHandler {

  /**
   * Array of properties skip on the Address field type.
   *
   * @var array
   */
  private $propertiesToSkip = ['langcode'];

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {
    $storage = $context->getPrimaryField()->getFieldDefinition()
      ->getFieldStorageDefinition();
    $cardinality = $storage->getCardinality();

    if ($cardinality == 1) {
      $primary_address = $context->getPrimaryField()->first();
      $secondary_address = $context->getSecondaryField()->first();

      $has_one = FALSE;

      if ($primary_address) {
        $primary_address = $primary_address->getValue();
        $has_one = TRUE;
      }
      if ($secondary_address) {
        $secondary_address = $secondary_address->getValue();
        $has_one = TRUE;
      }

      // Only bother showing the fields if either primary or secondary
      // have values.
      if ($has_one) {
        foreach ($storage->getPropertyDefinitions() as $property_id => $property) {
          if (in_array($property_id, $this->propertiesToSkip)) {
            continue;
          }

          $primary_value = $primary_address[$property_id] ?? NULL;
          $secondary_value = $secondary_address[$property_id] ?? NULL;

          $context->setFieldDisplayInfo('', $property_id, [
            'comparison' => $this->displayCompare($primary_value, $secondary_value, $context),
            'label' => $property->getLabel(),
            'primary' => $primary_value,
            'secondary' => $secondary_value,
          ]);
        }
        return FieldMergeHandlerInterface::HANDLED;
      }
    }
    return FieldMergeHandlerInterface::NOT_HANDLED;
  }

}
