<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\entity_merge\MergeContext;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\SubscriberInterface;

/**
 * Simplenews Subscriber entity merge handler.
 *
 * @EntityMergeHandler(
 *   id = "simplenews_subscriber",
 * )
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
class SubscriberEntityMergeHandler extends EntityMergeHandlerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkip(MergeContext $context): array {
    $keys = parent::getFieldsToSkip($context);
    // Add subscriptions field to be skipped.
    $keys['subscriptions'] = 'subscriptions';
    return $keys;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getDisplayFieldsToSkip($context), [
      'uuid',
      'uid',
      'mail',
      'language',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function postMerge(MergeContext $context): void {
    // Merge the subscriptions for the subscribers.
    /** @var \Drupal\simplenews\Entity\Subscriber $primary */
    $primary = $context->getPrimary();
    /** @var \Drupal\simplenews\Entity\Subscriber $secondary */
    $secondary = $context->getSecondary();

    if ($secondary->get('status')->value == SubscriberInterface::INACTIVE) {
      // Intentionally do nothing.
      return;
    }

    if ($primary->get('status')->value == SubscriberInterface::INACTIVE) {
      // Delete all primary subscriptions.
      foreach ($primary->getSubscribedNewsletterIds() as $newsletter_id) {
        $primary->unsubscribe($newsletter_id);
      }

      $primary->setStatus(SubscriberInterface::ACTIVE);
    }

    $this->mergeUniqueSubs($secondary, $primary);
    $secondary->setStatus(SubscriberInterface::INACTIVE);

  }

  /**
   * Copy unique subscriptions from one subscriber to another ensuring.
   *
   * @var \Drupal\simplenews\Entity\Subscriber $from
   *   Subscriber to copy subscriptions from
   * @var \Drupal\simplenews\Entity\Subscriber $to
   *   Subscriber to copy subscriptions to
   */
  private function mergeUniqueSubs(Subscriber $from, Subscriber $to): void {
    $existing_subscriptions = $to->getSubscribedNewsletterIds();
    foreach ($from->getSubscribedNewsletterIds() as $subscription) {
      if (!in_array($subscription, $existing_subscriptions)) {
        $to->subscribe($subscription);
      }
    }
  }

}
