<?php

namespace Drupal\entity_merge;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\entity_merge\Entity\MergeRequestInterface;
use Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager;
use Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager;

/**
 * Class used to being the merge process.
 *
 * @package Drupal\entity_merge
 */
class Merger {

  /**
   * Entity merge handler plugin manager.
   *
   * @var \Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager
   */
  private $pluginManager;

  /**
   * Field handler plugin manager.
   *
   * @var \Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager
   */
  private $fieldPluginManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * Merger constructor.
   *
   * @param \Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager $plugin_manager
   *   Merge handler plugin manager.
   * @param \Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager $field_plugin_manager
   *   Field handler plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   */
  public function __construct(EntityMergeHandlerPluginManager $plugin_manager, FieldMergeHandlerPluginManager $field_plugin_manager, EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    $this->pluginManager = $plugin_manager;
    $this->fieldPluginManager = $field_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * Runs the request using a merge request ID.
   *
   * @param int $id
   *   The ID of the merge request to run.
   *
   * @return bool
   *   Whether the merge was successful.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function runById($id) {
    if ($entity = $this->entityTypeManager->getStorage('entity_merge_request')->load($id)) {
      return $this->run($entity);
    }
    return FALSE;
  }

  /**
   * Runs a merge request.
   *
   * @param \Drupal\entity_merge\Entity\MergeRequest $request
   *   The merge request to run.
   *
   * @return bool
   *   Whether the result was successful.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function run(MergeRequest $request) {
    $result = FALSE;

    if ($request->getStatus() != MergeRequestInterface::STATUS_NOT_STARTED) {
      return $result;
    }

    try {
      $request->setStatus(MergeRequestInterface::STATUS_IN_PROGRESS);
      $request->save();

      $transaction = $this->connection->startTransaction();

      // Find a handler for our entity type.
      $primary_entity = $request->getPrimaryEntity();
      $secondary_entity = $request->getSecondaryEntity();

      if ($primary_entity === NULL) {
        // If the primary has been deleted before the merge request runs,
        // log this and mark as failed.
        $request->setStatus(MergeRequestInterface::STATUS_FAILED);
        $request->get('log')->appendItem([
          'message' => 'The primary entity was deleted before the merge could run.',
        ]);
      }
      elseif ($secondary_entity === NULL) {
        // If the secondary has been deleted before the merge request runs,
        // log this and mark as failed.
        $request->setStatus(MergeRequestInterface::STATUS_FAILED);
        // If the primary has been deleted before the merge request runs,
        // log this and mark as failed.
        $request->setStatus(MergeRequestInterface::STATUS_FAILED);
        $request->get('log')->appendItem([
          'message' => 'The secondary entity was deleted before the merge could run.',
        ]);
      }
      else {
        /** @var \Drupal\entity_merge\Plugin\EntityMergeHandlerInterface $handler */
        $handler = $this->pluginManager->createInstance($primary_entity->getEntityTypeId());

        $context = new MergeContext($request, $primary_entity, $request->getSecondaryEntity(), $this->fieldPluginManager, $this->entityTypeManager, $this->pluginManager);
        $handler->performMerge($context);

        if ($context->hasErrors()) {
          $transaction->rollBack();
          $request->setStatus(MergeRequestInterface::STATUS_FAILED);
        }
        else {
          $handler->repointEntityReferences($context);
          $request->setStatus(MergeRequestInterface::STATUS_SUCCESS);
          $result = TRUE;
        }

        $request->set('log', $context->getProgress());
      }

      $request->set('completed', time());
      $request->save();
      return $result;
    }
    catch (\Throwable $exception) {
      if (isset($transaction)) {
        $transaction->rollBack();
      }
      $params = Error::decodeException($exception);
      $request->set('log', ['message' => strtr('%type: @message', $params)]);
      $request->setStatus(MergeRequestInterface::STATUS_FAILED);
      $request->save();
      return FALSE;
    }
  }

  /**
   * Gets the fields to display.
   *
   * @param string $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\ContentEntityInterface $primary_entity
   *   The primary entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $secondary_entity
   *   The secondary entity.
   *
   * @return array
   *   Array of fields to display.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function displayFields($entity_type, ContentEntityInterface $primary_entity, ContentEntityInterface $secondary_entity) {
    $handler = $this->pluginManager->createInstance($entity_type);
    // Create a 'fake' merge request.
    $request = $request = MergeRequest::create([
      'status' => 'InProgress',
      'target_type' => $entity_type,
      'primary_id' => $primary_entity->id(),
      'secondary_id' => $secondary_entity->id(),
    ]);

    $context = new MergeContext($request, $primary_entity, $secondary_entity, $this->fieldPluginManager, $this->entityTypeManager, $this->pluginManager);

    return $handler->displayFields($context);
  }

}
