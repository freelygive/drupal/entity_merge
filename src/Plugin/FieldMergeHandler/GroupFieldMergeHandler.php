<?php

namespace Drupal\entity_merge\Plugin\FieldMergeHandler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles merging groups for organisations.
 *
 * @FieldMergeHandler(
 *   id = "merge_field_handler_group",
 *   handles_entity_type = "user",
 *   handles_field_type = "entity_reference"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class GroupFieldMergeHandler extends DefaultFieldMergeHandler implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GroupFieldMergeHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $referenced_entity_type = $context->getPrimaryField()->getFieldDefinition()
      ->getSetting('target_type');

    // Only process if this is a reference to a group entity, and its
    // the 'group' field on the user that we're looking at then we want
    // to kick off a sub merge for the group.
    if ($referenced_entity_type == 'group' && $context->getPrimaryField()->getName() == 'group') {
      /** @var \Drupal\user\Entity\User $user1 */
      /** @var \Drupal\user\Entity\User $user2 */
      // Safe to assume both entities being merged are users as this field
      // handler specifies handles_entity_type in the annotation.
      $user1 = $context->getParentContext()->getPrimary();
      $user2 = $context->getParentContext()->getSecondary();

      // Only proceed trying to merge groups if both entities being merged
      // are organisations.
      if ($user1->hasRole('crm_org') && $user2->hasRole('crm_org')) {
        /** @var \Drupal\group\Entity\Group $primary_group */
        /** @var \Drupal\group\Entity\Group $secondary_group */
        $primary_group = $user1->get($context->getFieldId())->entity;
        $secondary_group = $user2->get($context->getFieldId())->entity;

        if ($secondary_group) {
          // Ensure the primary group exists and has an ID.
          if ($primary_group->isNew()) {
            $primary_group->save();
          }

          // Loop over each group membership on the secondary
          // and assign it to the primary.
          foreach ($this->getGroupMemberships($secondary_group) as $group_membership) {
            $group_membership->set('gid', $primary_group->id());
            $group_membership->save();
          }
        }

        return self::HANDLED;
      }
    }
    return self::NOT_HANDLED;
  }

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {

    $referenced_entity_type = $context->getPrimaryField()->getFieldDefinition()
      ->getSetting('target_type');

    // Only process if this is a reference to a group entity, and its
    // the 'group' field on the user that we're looking at then we want
    // to kick off a sub merge for the group.
    if ($referenced_entity_type == 'group' && $context->getPrimaryField()->getName() == 'group') {
      /** @var \Drupal\user\Entity\User $user1 */
      /** @var \Drupal\user\Entity\User $user2 */
      // Safe to assume both entities being merged are users as this field
      // handler specifies handles_entity_type in the annotation.
      $user1 = $context->getParentContext()->getPrimary();
      $user2 = $context->getParentContext()->getSecondary();

      /** @var \Drupal\group\Entity\Group $primary_group */
      /** @var \Drupal\group\Entity\Group $secondary_group */
      $primary_group = $user1->get($context->getFieldId())->entity;
      $secondary_group = $user2->get($context->getFieldId())->entity;

      $primary_count = $primary_group ? count($this->getGroupMemberships($primary_group)) : 0;
      $secondary_count = $secondary_group ? count($this->getGroupMemberships($secondary_group)) : 0;

      // If either of them is an org, then display the field even though
      // we only actually do a merge if 1 of them is an organisation.
      if ($user1->hasRole('crm_org') || $user2->hasRole('crm_org')) {
        $context->setFieldDisplayInfo('Organisation Members', $context->getFieldId(), [
          'comparison' => NULL,
          'label' => new TranslatableMarkup('Number of Members'),
          'primary' => $primary_count,
          'secondary' => $secondary_count,
        ]);
      }

      return FieldMergeHandlerInterface::HANDLED;
    }

    return FieldMergeHandlerInterface::NOT_HANDLED;
  }

  /**
   * Gets the relationship entities for a group.
   *
   * This method checks whether the group entity is a group 1.x entity
   * or a group 2.x entity. This is necessary as the method to call is
   * different between the two, and allows entity_merge to be agnostic
   * about which version is installed and not require a hard dependency.
   *
   * @param \Drupal\Core\Entity\EntityInterface $group
   *   The group entity we're working with.
   *
   * @return \Drupal\group\Entity\GroupRelationship[]|\Drupal\group\Entity\GroupContent[]
   *   Array of group relationships. The actual type will vary depending
   *   on which version of Group is in use.
   */
  private function getGroupMemberships(EntityInterface $group) {

    if (method_exists($group, 'getRelationships')) {
      return $group->getRelationships();
    }
    elseif (method_exists($group, 'getContent')) {
      return $group->getContent();
    }
    return [];
  }

}
