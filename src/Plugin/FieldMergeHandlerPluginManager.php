<?php

namespace Drupal\entity_merge\Plugin;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Field merger plugin manager.
 *
 * @package Drupal\entity_merge\Plugin
 */
class FieldMergeHandlerPluginManager extends DefaultPluginManager {

  /**
   * Cached plugin instances.
   *
   * @var array
   */
  private $pluginCache = [];

  /**
   * Constructor for FieldMergePluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/FieldMergeHandler', $namespaces, $module_handler, 'Drupal\entity_merge\Plugin\FieldMergeHandlerInterface', 'Drupal\entity_merge\Annotation\FieldMergeHandler');

    $this->alterInfo('field_merge_handler_info');
    $this->setCacheBackend($cache_backend, 'field_merge_handler_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterDefinitions(&$definitions) {
    parent::alterDefinitions($definitions);

    // Sort our definitions by weight.
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);
  }

  /**
   * Fields field handler by field name.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $field_id
   *   Field name.
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface[]
   *   Matching handlers.
   */
  public function findByFieldName($entity_type, $field_id) {
    $matches = array_filter($this->getDefinitions(), function ($plugin) use ($field_id, $entity_type) {
      return isset($plugin['handles_entity_type']) && isset($plugin['handles_field_name']) && $plugin['handles_entity_type'] == $entity_type && $plugin['handles_field_name'] == $field_id;
    });

    uasort($matches, [SortArray::class, 'sortByWeightElement']);

    $plugins = array_map(function ($plugin) {
      if (array_key_exists($plugin['id'], $this->pluginCache)) {
        return $this->pluginCache[$plugin['id']];
      }
      else {
        $instance = $this->createInstance($plugin['id']);
        $this->pluginCache[$plugin['id']] = $instance;
        return $instance;
      }
    }, $matches);

    return $plugins;
  }

  /**
   * Finds field handler by field type.
   *
   * @param string $field_type
   *   Field type.
   * @param string $entity_type
   *   Entity type (optional).
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface[]
   *   Matching handlers.
   */
  public function findByFieldType($field_type, $entity_type = NULL) {
    $matches = array_filter($this->getDefinitions(), function ($plugin) use ($field_type, $entity_type) {
      $matches_entity = ($entity_type != NULL && isset($plugin['handles_entity_type']))
        || ($entity_type == NULL && !isset($plugin['handles_entity_type']));

      return $matches_entity && isset($plugin['handles_field_type']) && $plugin['handles_field_type'] == $field_type;
    });

    uasort($matches, [SortArray::class, 'sortByWeightElement']);

    $plugins = array_map(function ($plugin) {
      if (array_key_exists($plugin['id'], $this->pluginCache)) {
        return $this->pluginCache[$plugin['id']];
      }
      else {
        $instance = $this->createInstance($plugin['id']);
        $this->pluginCache[$plugin['id']] = $instance;
        return $instance;
      }
    }, $matches);

    return $plugins;
  }

  /**
   * Gets the default field merge handler.
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface
   *   Default field merge handler.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getDefaultFieldHandler() {
    if (empty($this->pluginCache['default'])) {
      $this->pluginCache['default'] = $this->createInstance('default');
    }
    return $this->pluginCache['default'];
  }

}
