<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\entity_merge\Entity\MergeRequestInterface;
use Drupal\simplenews\SubscriberInterface;

/**
 * Test coverage for the Simplenews subscriber entity merge handler.
 *
 * @group legacy
 *
 * @coversDefaultClass \Drupal\entity_merge\Plugin\FieldMergeHandler\SubscriptionFieldMergeHandler
 */
class SimplenewsSubscriberEntityMergeHandlerTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_merge',
    'options',
    'user',
    'node',
    'field',
    'system',
    'simplenews',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    // Lots of simplenews config fails schema checks.
    'field.storage.simplenews_subscriber.subscriptions',
    'field.field.simplenews_subscriber.simplenews_subscriber.subscriptions',
    'views.view.simplenews_subscribers',
  ];

  /**
   * A newsletter to test with.
   *
   * @var \Drupal\simplenews\NewsletterInterface
   */
  protected $newsletter1;

  /**
   * A second newsletter to test with.
   *
   * @var \Drupal\simplenews\NewsletterInterface
   */
  protected $newsletter2;

  /**
   * The subscriber entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $subscriberStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('entity_merge_request');
    $this->installEntitySchema('simplenews_subscriber');
    $this->installEntitySchema('simplenews_subscriber_history');

    $this->installSchema('system', 'sequences');
    $this->installSchema('simplenews', 'simplenews_mail_spool');
    $this->installConfig('simplenews');

    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->subscriberStorage = $entity_type_manager->getStorage('simplenews_subscriber');

    $newsletter_storage = $entity_type_manager->getStorage('simplenews_newsletter');
    $this->newsletter1 = $newsletter_storage->create(['id' => 'newsletter_1']);
    $this->newsletter1->save();
    $this->newsletter2 = $newsletter_storage->create(['id' => 'newsletter_2']);
    $this->newsletter2->save();

    $time = $this->prophesize(TimeInterface::class);
    $time->getRequestTime()->willReturn(150);
    $time->getCurrentTime()->willReturn(160);
    $this->container->set('datetime.time', $time->reveal());
  }

  /**
   * Test merging the subscription field data.
   *
   * @param array $subs_1
   *   User 1's initial subscriptions.
   * @param array $subs_2
   *   User 2's initial subscriptions.
   * @param int $status_1
   *   First subscriber's initial status.
   * @param int $status_2
   *   Second subscriber's initial status.
   * @param array $expected_subs_1
   *   User 1's final subscriptions.
   * @param array $expected_subs_2
   *   User 2's final subscriptions.
   * @param int $expected_status_1
   *   First subscriber's final status.
   * @param int $expected_status_2
   *   Second subscriber's final status.
   *
   * @dataProvider dataMerge
   * @covers ::performMerge
   */
  public function testMerge(array $subs_1, array $subs_2, int $status_1, int $status_2, array $expected_subs_1, array $expected_subs_2, int $expected_status_1, int $expected_status_2) {
    // Create the subscriber 1 and it's subscriptions.
    $subscriber_1 = $this->subscriberStorage->create();
    foreach ($subs_1 as $sub) {
      $subscriber_1->get('subscriptions')->appendItem($sub);
    }
    $subscriber_1->set('status', $status_1);
    $subscriber_1->save();

    // Create the subscriber 2 and it's subscriptions.
    $subscriber_2 = $this->subscriberStorage->create();
    foreach ($subs_2 as $sub) {
      $subscriber_2->get('subscriptions')->appendItem($sub);
    }
    $subscriber_2->set('status', $status_2);
    $subscriber_2->save();

    // Run the merge.
    $request = MergeRequest::create([
      'primary_id' => $subscriber_1->id(),
      'secondary_id' => $subscriber_2->id(),
      'target_type' => 'simplenews_subscriber',
      'status' => MergeRequestInterface::STATUS_NOT_STARTED,
    ]);
    $result = $this->container->get('entity_merge.merger')->run($request);
    $this->assertTrue($result, 'Merge succeeded');

    // Check that we have the correct subscribers/status for subscriber 1.
    $subscriber_1 = $this->subscriberStorage->loadUnchanged($subscriber_1->id());
    $subscriptions = $subscriber_1->get('subscriptions')->getValue();
    $this->assertEquals($expected_subs_1, $subscriptions, 'Subscriber 1 subscriptions');
    $this->assertEquals($expected_status_1, $subscriber_1->get('status')->value);

    // Check that we have the correct subscribers/status for subscriber 2.
    $subscriber_2 = $this->subscriberStorage->loadUnchanged($subscriber_2->id());
    $subscriptions = $subscriber_2->get('subscriptions')->getValue();
    $this->assertEquals($expected_subs_2, $subscriptions, 'Subscriber 2 subscriptions');
    $this->assertEquals($expected_status_2, $subscriber_2->get('status')->value);
  }

  /**
   * Data provider for testing subscriber merges.
   */
  protected static function dataMerge() {
    // Only the subscriber has any subscriptions.
    // Secondary ends up deactivated.
    yield 'only-primary' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'subs_2' => [],
      'status_1' => SubscriberInterface::ACTIVE,
      'status_2' => SubscriberInterface::ACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_subs_2' => [],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // Only the secondary subscriber has any subscriptions.
    // They're copied across to the primary, secondary is then deactivated.
    yield 'only-secondary' => [
      'subs_1' => [],
      'subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'status_1' => SubscriberInterface::ACTIVE,
      'status_2' => SubscriberInterface::ACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // Both subscribers have a single identical subscription.
    // No changes made to subscriptions, secondary is then deactivated.
    yield 'single-identical' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'status_1' => SubscriberInterface::ACTIVE,
      'status_2' => SubscriberInterface::ACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // Subscriber 2 has some subscriptions subscriber 1 does not.
    // Those that are missing are copied across. Secondary is then deactivated.
    yield 'multiple-differing' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'status_1' => SubscriberInterface::ACTIVE,
      'status_2' => SubscriberInterface::ACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_1',
        ],
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // Primary subscriber is active, secondary is inactive.
    // No action required.
    yield 'primary-active-secondary-inactive' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'subs_2' => [
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'status_1' => SubscriberInterface::ACTIVE,
      'status_2' => SubscriberInterface::INACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // The primary subscriber is inactive but the secondary is active.
    // The primary's subscriptions should be cleared out,
    // the secondary's should be copied across
    // the primary is then activated and the secondary is deactivated.
    yield 'primary-inactive-secondary-active' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'subs_2' => [
        [
          'target_id' => 'newsletter_3',
        ],
      ],
      'status_1' => SubscriberInterface::INACTIVE,
      'status_2' => SubscriberInterface::ACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_3',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_3',
        ],
      ],
      'expected_status_1' => SubscriberInterface::ACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];

    // Primary subscriber is active, secondary is inactive.
    // No action required.
    yield 'primary-inactive-secondary-inactive' => [
      'subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'subs_2' => [
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'status_1' => SubscriberInterface::INACTIVE,
      'status_2' => SubscriberInterface::INACTIVE,
      'expected_subs_1' => [
        [
          'target_id' => 'newsletter_1',
        ],
      ],
      'expected_subs_2' => [
        [
          'target_id' => 'newsletter_2',
        ],
      ],
      'expected_status_1' => SubscriberInterface::INACTIVE,
      'expected_status_2' => SubscriberInterface::INACTIVE,
    ];
  }

}
