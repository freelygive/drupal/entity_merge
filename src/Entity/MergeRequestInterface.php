<?php

namespace Drupal\entity_merge\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Merge request entities.
 *
 * @ingroup entity_merge
 */
interface MergeRequestInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Indicates merge request has failed.
   */
  const STATUS_FAILED = 'failed';

  /**
   * Indicates merge request has succeeded.
   */
  const STATUS_SUCCESS = 'success';

  /**
   * Indicates merge request is in progress.
   */
  const STATUS_IN_PROGRESS = 'in_progress';

  /**
   * Indicates merge request has not started.
   */
  const STATUS_NOT_STARTED = 'not_started';

  /**
   * Gets the Merge request creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Merge request.
   */
  public function getCreatedTime();

  /**
   * Sets the Merge request creation timestamp.
   *
   * @param int $timestamp
   *   The Merge request creation timestamp.
   *
   * @return \Drupal\entity_merge\Entity\MergeRequestInterface
   *   The called Merge request entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Sets the secondary entity.
   *
   * @param string $secondary_id
   *   The ID.
   */
  public function setSecondary($secondary_id);

  /**
   * Sets the primary entity.
   *
   * @param string $primary_id
   *   The ID.
   */
  public function setPrimary($primary_id);

  /**
   * Gets the status.
   *
   * @return string
   *   The status.
   */
  public function getStatus();

  /**
   * Get the status label.
   *
   * @return string
   *   The status label.
   */
  public function getStatusLabel();

  /**
   * Sets the target entity type.
   *
   * @param string $type
   *   The entity type.
   */
  public function setTargetType($type);

  /**
   * Gets the primary entity.
   *
   * Note this is not using entity_reference field types as we don't know
   * the target entity type ahead of time.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The primary entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getPrimaryEntity();

  /**
   * Sets the status.
   *
   * @param string $value
   *   The status.
   */
  public function setStatus($value);

  /**
   * Gets the secondary entity.
   *
   * Note this is not using entity_reference field types as we don't know
   * the target entity type ahead of time.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The secondary entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getSecondaryEntity();

  /**
   * Gets the target entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getTargetType();

  /**
   * Whether the merge request is in progress.
   *
   * @return bool
   *   Whether the merge request is in progress.
   */
  public function isInProgress() : bool;

}
