<?php

namespace Drupal\entity_merge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\entity_merge\Entity\MergeRequestInterface;
use Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager;
use Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager;

/**
 * Context object passed through the merge process.
 *
 * @package Drupal\entity_merge
 */
class MergeContext {

  /**
   * The original merge request.
   *
   * @var \Drupal\entity_merge\Entity\MergeRequest
   */
  private $request;

  /**
   * Merge progress.
   *
   * @var array
   */
  private $progress = [];

  /**
   * Prefix to use for field names when tracking progress.
   *
   * @var string
   */
  private $progressPrefix = '';

  /**
   * Whether any errors have been raised.
   *
   * @var bool
   */
  private $hasErrors;

  /**
   * Field plugin manager.
   *
   * @var \Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager
   */
  private $fieldPluginManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Primary entity to merge.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  private $primary;

  /**
   * Secondary entity to merge.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  private $secondary;

  /**
   * Entity plugin manager.
   *
   * @var \Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager
   */
  private $entityPluginManager;

  /**
   * Parent Merge Context.
   *
   * @var \Drupal\entity_merge\MergeContext
   */
  private $parentContext;

  /**
   * MergeContext constructor.
   *
   * @param \Drupal\entity_merge\Entity\MergeRequest $request
   *   Merge request.
   * @param \Drupal\Core\Entity\ContentEntityInterface $primary
   *   Primary entity being merged.
   * @param \Drupal\Core\Entity\ContentEntityInterface $secondary
   *   Secondary entity being merged.
   * @param \Drupal\entity_merge\Plugin\FieldMergeHandlerPluginManager $field_plugin_manager
   *   Plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager.
   * @param \Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager $entity_plugin_manager
   *   Plugin manager.
   */
  public function __construct(MergeRequest $request, ContentEntityInterface $primary, ContentEntityInterface $secondary, FieldMergeHandlerPluginManager $field_plugin_manager, EntityTypeManagerInterface $entity_type_manager, EntityMergeHandlerPluginManager $entity_plugin_manager) {
    $this->request = $request;
    $this->primary = $primary;
    $this->secondary = $secondary;
    $this->fieldPluginManager = $field_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityPluginManager = $entity_plugin_manager;
  }

  /**
   * Fields field handler by field name.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $field_id
   *   Field name.
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface[]
   *   Matching handlers.
   */
  public function findFieldHandlersByFieldName($entity_type, $field_id) {
    return $this->fieldPluginManager->findByFieldName($entity_type, $field_id);
  }

  /**
   * Finds field handler by field type.
   *
   * @param string $field_type
   *   Field type.
   * @param string $entity_type
   *   Entity type (optional).
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface[]
   *   Matching handlers.
   */
  public function findFieldHandlersByFieldType($field_type, $entity_type = NULL) {
    return $this->fieldPluginManager->findByFieldType($field_type, $entity_type);
  }

  /**
   * Gets the default field merge handler.
   *
   * @return \Drupal\entity_merge\Plugin\FieldMergeHandlerInterface
   *   Default field merge handler.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getDefaultFieldHandler() {
    return $this->fieldPluginManager->getDefaultFieldHandler();
  }

  /**
   * Adds progress message to the log.
   *
   * @param string $message
   *   The message.
   * @param string $field
   *   The field name.
   * @param mixed|null $primary_value
   *   Primary entity's field value.
   * @param mixed|null $secondary_value
   *   Secondary entity's field value.
   */
  public function addProgress($message, $field = NULL, $primary_value = NULL, $secondary_value = NULL) {
    $this->progress[] = [
      'field' => $this->progressPrefix . $field,
      'primary_value' => $primary_value,
      'secondary_value' => $secondary_value,
      'message' => $message,
    ];
  }

  /**
   * Adds an error message to the log.
   *
   * @param string $error
   *   The error message.
   * @param string $field
   *   The field name.
   * @param mixed|null $primary_value
   *   Primary entity's field value.
   * @param mixed|null $secondary_value
   *   Secondary entity's field value.
   */
  public function addError($error, $field = NULL, $primary_value = NULL, $secondary_value = NULL) {
    $this->hasErrors = TRUE;
    $this->addProgress('ERROR - ' . $error, $field, $primary_value, $secondary_value);
  }

  /**
   * Whether there is an error.
   *
   * @return bool
   *   Whether there are any errors.
   */
  public function hasErrors() {
    return $this->hasErrors;
  }

  /**
   * Gets the log.
   *
   * @return array
   *   The progress log.
   */
  public function getProgress() {
    return $this->progress;
  }

  /**
   * Clones the context for use in a child merge request.
   *
   * @param string $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\ContentEntityInterface $new_primary
   *   Primary entity to merge.
   * @param \Drupal\Core\Entity\ContentEntityInterface $new_secondary
   *   Secondary entity to merge.
   * @param string $field
   *   The field name the child context is for, used to track nested progress.
   *
   * @return static
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function createChildContext($entity_type, ContentEntityInterface $new_primary, ContentEntityInterface $new_secondary, $field = '') {
    // Create a new merge request for our new primary/secondary.
    // This request won't ever be saved.
    $request = MergeRequest::create([
      'status' => MergeRequestInterface::STATUS_IN_PROGRESS,
      'target_type' => $entity_type,
      'primary_id' => $new_primary->id(),
      'secondary_id' => $new_secondary->id(),
    ]);

    $new_context = new static($request, $new_primary, $new_secondary, $this->fieldPluginManager, $this->entityTypeManager, $this->entityPluginManager);
    $new_context->progress = &$this->progress;
    $new_context->hasErrors = &$this->hasErrors;
    $new_context->parentContext = $this;
    $new_context->progressPrefix = $this->progressPrefix . ($field ? $field . '.' : '');
    return $new_context;
  }

  /**
   * Primary entity being merged.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Primary entity being merged.
   */
  public function getPrimary(): ContentEntityInterface {
    return $this->primary;
  }

  /**
   * Secondary entity being merged.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Secondary entity being merged.
   */
  public function getSecondary(): ContentEntityInterface {
    return $this->secondary;
  }

  /**
   * Gets the entity merge handler plugin manager.
   *
   * @return \Drupal\entity_merge\Plugin\EntityMergeHandlerPluginManager
   *   Gets the entity merge handler plugin manager.
   */
  public function getEntityPluginManager(): EntityMergeHandlerPluginManager {
    return $this->entityPluginManager;
  }

  /**
   * Gets the parent context if there is one.
   *
   * @return \Drupal\entity_merge\MergeContext|null
   *   The parent context, or null if this is a root-level merge.
   */
  public function getParentContext() : ?MergeContext {
    return $this->parentContext;
  }

}
