<?php

namespace Drupal\entity_merge\Plugin\EntityMergeHandler;

use Drupal\entity_merge\MergeContext;

/**
 * Field Collection Item entity merge handler.
 *
 * @EntityMergeHandler(
 *   id = "field_collection_item",
 * )
 *
 * @package Drupal\entity_merge\Plugin\EntityMergeHandler
 */
class FieldCollectionEntityMergeHandler extends EntityMergeHandlerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getFieldsToSkip($context), [
      'host_id',
      'host_type',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDisplayFieldsToSkip(MergeContext $context): array {
    return array_merge(parent::getDisplayFieldsToSkip($context), [
      'host_id',
      'host_type',
    ]);
  }

}
