<?php

namespace Drupal\entity_merge\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a field merge handler annotation.
 *
 * @package Drupal\entity_merge\Annotation
 *
 * @Annotation
 */
class FieldMergeHandler extends Plugin {

  /**
   * Plugin ID.
   *
   * @var string
   */
  public $id;

  //@codingStandardsIgnoreStart
  // Coding standards intentionally ignored as underscores in annotation
  // fields is convention.
  /**
   * The entity type this field handler acts upon. Blank for all entities.
   *
   * @var string
   */
  public $handles_entity_type;

  /**
   * The field name that this handler acts upon.
   *
   * If set, handles_entity_type must be set too.
   *
   * @var string
   */
  public $handles_field_name;

  /**
   * The field type this handler acts upon.
   *
   * @var string
   */
  public $handles_field_type;
  //@codingStandardsIgnoreEnd
}
