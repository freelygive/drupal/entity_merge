<?php

namespace Drupal\entity_merge\Plugin\FieldMergeHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles merging profile fields.
 *
 * @FieldMergeHandler(
 *   id = "merge_field_handler_profile",
 *   handles_field_type = "entity_reference"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class ProfileReferenceMergeHandler implements FieldMergeHandlerInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * ProfileReferenceMergeHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container->get('config.factory'), $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();
    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      $context->addProgress('Computed field - not attempting to merge.');
      return self::HANDLED;
    }

    // For profile references, check if they allow multiple or single.
    // If they allow multiple, we don't need to do anything as the standard
    // entity merge handler will do the work.
    $referenced_entity_type = $field_definition->getSetting('target_type');

    if ($referenced_entity_type == 'profile') {
      if ($this->singleInstanceOnly($field_definition)) {
        /** @var \Drupal\profile\Entity\ProfileInterface $primary_profile */
        $primary_profile = $context->getParentContext()->getPrimary()->get($context->getFieldId())->entity;
        /** @var \Drupal\profile\Entity\ProfileInterface $secondary_profile */
        $secondary_profile = $context->getParentContext()->getSecondary()->get($context->getFieldId())->entity;

        // If secondary doesn't have one, there's nothing to do.
        if ($secondary_profile == NULL) {
          $context->addProgress('No secondary profile');
          return self::HANDLED;
        }

        // If primary doesn't have one, re-assign secondary's profile
        // to primary. But only if the parent is a user.
        if ($primary_profile == NULL && $context->getParentContext()->getPrimary()->getEntityTypeId() == 'user') {
          $bundle = $secondary_profile->bundle();
          // We need to use the 'unchanged' version of the parent entity as
          // otherwise we'll end up triggering a save of the primary if other
          // fields have been merged. We don't want to do that at this point
          // as the save of the primary/secondary is done at the end.
          $original = $this->entityTypeManager->getStorage('user')
            ->loadUnchanged($context->getParentContext()->getPrimary()->id());

          $secondary_profile->setOwner($original);
          $secondary_profile->save();

          $this->updateProfileFields($context->getParentContext()->getPrimary(), $bundle);
          $this->updateProfileFields($context->getParentContext()->getSecondary(), $bundle);
          $context->addProgress('Transferred secondary profile to primary user.', NULL, $secondary_profile->id());
          return self::HANDLED;
        }

        // If they both have one, we want to set off a new merge process
        // for these two entities.
        $context->addProgress('Merging profiles.', $primary_profile->id(), $secondary_profile->id());
        $new_context = $context->getParentContext()->createChildContext('profile', $primary_profile, $secondary_profile, "profile:{$context->getFieldId()}");
        $handler = $new_context->getEntityPluginManager()->createInstance('profile');
        $handler->performMerge($new_context);

        return self::HANDLED;
      }
      else {
        $context->addProgress('Skipped multi-cardinality profile.');
      }
    }

    return self::NOT_HANDLED;
  }

  /**
   * Updates profile fields on Decoupled Auth users.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param string $profile_bundle
   *   The profile.
   */
  private function updateProfileFields(UserInterface $user, $profile_bundle) {
    // Only do this if we're using decoupled_auth.
    if (get_class($user) == 'Drupal\decoupled_auth\Entity\DecoupledAuthUser') {
      // updateProfileFields usually does a save on the user.
      // We don't want to do this as we want to save only at the end of the
      // merge process.
      $user->updateProfileFields([$profile_bundle], FALSE);
    }
  }

  /**
   * Checks whether a profile is single instance only.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   *
   * @return bool
   *   Whether a profile is single instance only.
   */
  protected function singleInstanceOnly(FieldDefinitionInterface $field_definition) {
    $handler_settings = $field_definition
      ->getSetting("handler_settings");

    if (!empty($handler_settings['target_bundles'])) {
      $profile_type = reset($handler_settings['target_bundles']);

      if (!empty($profile_type)) {
        $multiple = $this->configFactory->get('profile.type.' . $profile_type)
          ->get('multiple');
        return !$multiple;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();

    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      return self::HANDLED;
    }

    // For profile references, if they're single instance then display
    // all of their sub-fields in a group named after the profile type.
    $referenced_entity_type = $field_definition->getSetting('target_type');

    if ($referenced_entity_type == 'profile' && $this->singleInstanceOnly($field_definition)) {
      /** @var \Drupal\profile\Entity\Profile $primary_child */
      /** @var \Drupal\profile\Entity\Profile $secondary_child */
      $primary_child = $context->getPrimary()->get($context->getFieldId())->entity;
      $secondary_child = $context->getSecondary()->get($context->getFieldId())->entity;

      if ($primary_child != NULL || $secondary_child != NULL) {
        // If one or other is missing, create a fake empty profile
        // for comparison.
        if ($secondary_child == NULL) {
          $secondary_child = $this->entityTypeManager->getStorage('profile')
            ->create(['type' => $primary_child->bundle()]);
        }
        elseif ($primary_child == NULL) {
          $primary_child = $this->entityTypeManager->getStorage('profile')
            ->create(['type' => $secondary_child->bundle()]);
        }

        $new_context = $context->getParentContext()->createChildContext('profile', $primary_child, $secondary_child);
        $handler = $new_context->getEntityPluginManager()->createInstance('profile');
        $result = $handler->displayFields($new_context);

        foreach ($result as $group => $field_group) {
          // Make sure we replace the default group
          // with the current profile label.
          if ($group == '') {
            $group = (string) $field_definition->getLabel();
          }

          foreach ($field_group as $field_id => $field_display) {
            $context->setFieldDisplayInfo($group, $field_id, $field_display);
          }
        }
        return FieldMergeHandlerInterface::HANDLED;
      }
    }
    return FieldMergeHandlerInterface::NOT_HANDLED;
  }

}
