<?php

namespace Drupal\entity_merge\Plugin\FieldMergeHandler;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_merge\FieldDisplayContext;
use Drupal\entity_merge\FieldMergeContext;
use Drupal\entity_merge\Plugin\FieldMergeHandlerInterface;

/**
 * Default FieldMergeHandler.
 *
 * @FieldMergeHandler(
 *   id = "default"
 * )
 *
 * @package Drupal\entity_merge\Plugin\FieldMergeHandler
 */
class DefaultFieldMergeHandler implements FieldMergeHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function performMerge(FieldMergeContext $context) {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();

    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      $context->addProgress('Computed field - not attempting to merge.');
      return self::HANDLED;
    }

    // Put all the field values into one items array. We make sure we don't have
    // too many items later.
    $new_items = array_merge_recursive($context->getPrimaryField()->getValue(), $context->getSecondaryField()->getValue());

    foreach ($new_items as $index => $value) {
      if (empty($value) || $this->isEmpty($value, $context)) {
        unset($new_items[$index]);
      }
    }

    $this->processFieldValues($new_items, $context);

    // Re-index the items.
    $new_items = array_values($new_items);

    $cardinality = $field_definition
      ->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality != FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      if (count($new_items) > $cardinality) {
        $new_items = array_slice($new_items, 0, $cardinality);
      }
    }

    if (count($context->getPrimaryField()->getValue()) != count($new_items)) {
      $context->getParentContext()->addProgress('Merging values', $context->getFieldId());
    }

    $context->getParentContext()->getPrimary()->set($context->getFieldId(), !empty($new_items) ? $new_items : NULL);

    return self::HANDLED;
  }

  /**
   * Whether the field is empty.
   *
   * @param mixed $value
   *   Field value.
   * @param \Drupal\entity_merge\FieldMergeContext $context
   *   Merge context.
   *
   * @return bool
   *   Whether the field is empty.
   */
  protected function isEmpty($value, FieldMergeContext $context) {
    return FALSE;
  }

  /**
   * Allows additional processing/massaging of field values.
   *
   * @param array $values
   *   Field value array.
   * @param \Drupal\entity_merge\FieldMergeContext $context
   *   Merge context.
   */
  protected function processFieldValues(array &$values, FieldMergeContext $context) {
    $existing_values = [];

    foreach ($values as $delta => $item) {
      if (in_array($item, $existing_values) || empty($item)) {
        unset($values[$delta]);
      }
      else {
        $existing_values[] = $item;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function displayField(FieldDisplayContext $context) : bool {
    $field_definition = $context->getPrimaryField()->getFieldDefinition();

    // If the field is computed, skip it.
    if ($field_definition->isComputed()) {
      return self::HANDLED;
    }

    $primary_value = $context->getPrimaryValue();
    $secondary_value = $context->getSecondaryValue();

    $context->setFieldDisplayInfo('', $context->getFieldId(), [
      'comparison' => $this->displayCompare($primary_value, $secondary_value, $context),
      'label' => $field_definition->getLabel(),
      'primary' => empty($primary_value) ? NULL : $context->getPrimaryField(),
      'secondary' => empty($secondary_value) ? NULL : $context->getSecondaryField(),
    ]);

    return FieldMergeHandlerInterface::HANDLED;
  }

  /**
   * Compares the values from the primary and secondary entity.
   *
   * @param mixed $primary_value
   *   Value from primary entity.
   * @param mixed $secondary_value
   *   Value from secondary entity.
   * @param \Drupal\entity_merge\FieldDisplayContext $context
   *   Current context.
   *
   * @return bool|null
   *   TRUE if the values are considered a match.
   *   FALSE if there's a conflict.
   *   NULL if a comparison was not run.
   */
  protected function displayCompare($primary_value, $secondary_value, FieldDisplayContext $context) : ?bool {
    $match = NULL;

    if ((is_array($primary_value) && !empty($primary_value) && !empty($secondary_value))
      || (!is_array($primary_value) && $primary_value !== NULL && $secondary_value !== NULL)) {
      $match = $primary_value === $secondary_value;
    }
    elseif ((is_array($primary_value) && empty($primary_value) && !empty($secondary_value))
      || (!is_array($primary_value) && $primary_value === NULL && $secondary_value)) {
      // For the purpose of displaying, where primary has no value and secondary
      // does have a value, treat them as equal so no highlighting is applied.
      $match = TRUE;
    }
    return $match;
  }

}
