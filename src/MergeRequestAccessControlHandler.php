<?php

namespace Drupal\entity_merge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Merge request entity.
 *
 * @see \Drupal\entity_merge\Entity\MergeRequest.
 */
class MergeRequestAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_merge\Entity\MergeRequest $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view all merge requests')
          ->orIf(AccessResult::allowedIfHasPermission($account, "merge {$entity->target_type->value} entities"));

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete merge requests')
          ->orIf(AccessResult::forbiddenIf($entity->isInProgress())->addCacheableDependency($entity));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // We do not have bundles, so if we have been given one, assume it is
    // the entity type we are attempting to merge, so we wont pass it on.
    $result = parent::checkCreateAccess($account, $context, NULL);
    // If user has the admin permission, always allow create access.
    if (!$result->isNeutral()) {
      return $result;
    }

    $merge_entity_type = $entity_bundle ?:
      $context['entity_type'] ??
      \Drupal::routeMatch()->getParameter('entity_type');

    if ($merge_entity_type) {
      $result = $result->orIf(AccessResult::allowedIfHasPermission($account, "merge $merge_entity_type entities")
        ->setCacheMaxAge(0));
    }

    return $result;
  }

}
