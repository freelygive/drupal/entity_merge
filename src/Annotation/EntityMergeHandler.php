<?php

namespace Drupal\entity_merge\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an entity merge handler.
 *
 * @package Drupal\entity_merge\Annotation
 *
 * @Annotation
 */
class EntityMergeHandler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
