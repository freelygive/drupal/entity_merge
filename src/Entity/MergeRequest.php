<?php

namespace Drupal\entity_merge\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Merge request entity.
 *
 * @ingroup entity_merge
 *
 * @ContentEntityType(
 *   id = "entity_merge_request",
 *   label = @Translation("Merge request"),
 *   label_collection = @Translation("Merge requests"),
 *   label_singular = @Translation("merge request"),
 *   label_plural = @Translation("merge requests"),
 *   label_count = @PluralTranslation(
 *     singular = "@count merge request",
 *     plural = "@count merge request",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_merge\MergeRequestListBuilder",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\entity_merge\MergeRequestAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\entity_merge\MergeRequestHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "entity_merge_request",
 *   admin_permission = "administer merge requests",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "created_by",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/merges/{entity_merge_request}",
 *     "delete-form" = "/admin/content/merges/{entity_merge_request}/delete",
 *     "collection" = "/admin/content/merges",
 *     "add-form" = "/admin/content/merges/{entity_type}/add",
 *     "verify" = "/admin/content/merges/{entity_type}/add/{primary_id}/{secondary_id}",
 *   },
 * )
 */
class MergeRequest extends ContentEntityBase implements MergeRequestInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'created_by' => \Drupal::currentUser()->id(),
      'status' => MergeRequestInterface::STATUS_NOT_STARTED,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('created_by')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('created_by')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('created_by', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('created_by', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusLabel() {
    $value = $this->getStatus();
    $provider = $this->get('status')
      ->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getOptionsProvider('value', $this);
    $options = OptGroup::flattenOptions($provider->getPossibleOptions());
    return $options[$value] ?? $value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($value) {
    $this->set('status', $value);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Created by'))
      ->setDescription(new TranslatableMarkup('The user ID of author of the Merge request entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'author',
        'label' => 'before',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setSettings([
        'allowed_values' => [
          MergeRequestInterface::STATUS_NOT_STARTED => new TranslatableMarkup('Not Started'),
          MergeRequestInterface::STATUS_IN_PROGRESS => new TranslatableMarkup('In Progress'),
          MergeRequestInterface::STATUS_SUCCESS => new TranslatableMarkup('Success'),
          MergeRequestInterface::STATUS_FAILED => new TranslatableMarkup('Failed'),
        ],
      ])
      ->setLabel(new TranslatableMarkup('Status of the merge request.'))
      ->setDescription(new TranslatableMarkup('Indicates the status of the merge request'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['target_type'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Entity type'))
      ->setDescription(new TranslatableMarkup('Type of entity to merge.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['primary_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Primary'))
      ->setDescription(new TranslatableMarkup('The primary entity to merge.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['secondary_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Secondary'))
      ->setDescription(new TranslatableMarkup('The secondary entity to merge.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['log'] = BaseFieldDefinition::create('entity_merge_log_entry')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(new TranslatableMarkup('Log'))
      ->setDescription(new TranslatableMarkup('Merge log'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the merge request finished.'))
      ->setDisplayOptions('view', [
        'type' => 'default',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

  /**
   * Gets the primary entity.
   *
   * Note this is not using entity_reference field types as we don't know
   * the target entity type ahead of time.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The primary entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getPrimaryEntity() {
    $id = $this->get('primary_id')->value;
    return $this->entityTypeManager()->getStorage($this->getTargetType())->load($id);
  }

  /**
   * Gets the secondary entity.
   *
   * Note this is not using entity_reference field types as we don't know
   * the target entity type ahead of time.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The secondary entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getSecondaryEntity() {
    $id = $this->get('secondary_id')->value;
    return $this->entityTypeManager()->getStorage($this->getTargetType())->load($id);
  }

  /**
   * Sets the primary entity.
   *
   * @param string $primary_id
   *   The ID.
   */
  public function setPrimary($primary_id) {
    $this->set('primary_id', $primary_id);
  }

  /**
   * Sets the secondary entity.
   *
   * @param string $secondary_id
   *   The ID.
   */
  public function setSecondary($secondary_id) {
    $this->set('secondary_id', $secondary_id);
  }

  /**
   * Sets the target entity type.
   *
   * @param string $type
   *   The entity type.
   */
  public function setTargetType($type) {
    $this->set('target_type', $type);
  }

  /**
   * Gets the target entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getTargetType() {
    return $this->get('target_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isInProgress() : bool {
    return $this->getStatus() == MergeRequestInterface::STATUS_IN_PROGRESS;
  }

}
