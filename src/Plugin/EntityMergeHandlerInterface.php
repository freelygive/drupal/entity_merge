<?php

namespace Drupal\entity_merge\Plugin;

use Drupal\entity_merge\MergeContext;

/**
 * Interface that defines an entity merge handler.
 *
 * @package Drupal\entity_merge\Plugin
 */
interface EntityMergeHandlerInterface {

  /**
   * Array of fields that should be rendered in the preview screen.
   *
   * @return array
   *   Fields to render.
   */
  public function displayFields(MergeContext $context);

  /**
   * Performs a merge.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Merge context representing the merge.
   */
  public function performMerge(MergeContext $context);

  /**
   * Re-points any entity references to the secondary entity to the primary.
   *
   * @param \Drupal\entity_merge\MergeContext $context
   *   Context.
   */
  public function repointEntityReferences(MergeContext $context);

}
