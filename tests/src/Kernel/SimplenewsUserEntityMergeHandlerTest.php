<?php

namespace Drupal\Tests\entity_merge\Kernel;

use Drupal\user\UserInterface;

/**
 * Test coverage for the subscriber aspects of the User entity merge handler.
 *
 * @group legacy
 *
 * @coversDefaultClass \Drupal\entity_merge\Plugin\EntityMergeHandler\UserEntityMergeHandler
 */
class SimplenewsUserEntityMergeHandlerTest extends EntityMergeTestBase {

  use DecoupledAuthUserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'decoupled_auth',
    'entity_merge',
    'options',
    'user',
    'node',
    'field',
    'system',
    'simplenews',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    // Lots of simplenews config fails schema checks.
    'field.storage.simplenews_subscriber.subscriptions',
    'field.field.simplenews_subscriber.simplenews_subscriber.subscriptions',
    'views.view.simplenews_subscribers',
  ];

  /**
   * The subscriber entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $subscriberStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('entity_merge_request');
    $this->installEntitySchema('simplenews_subscriber');
    $this->installEntitySchema('simplenews_subscriber_history');

    $this->installSchema('system', 'sequences');
    $this->installSchema('simplenews', 'simplenews_mail_spool');
    $this->installConfig('simplenews');

    $this->subscriberStorage = $this->container
      ->get('entity_type.manager')
      ->getStorage('simplenews_subscriber');
  }

  /**
   * Test that subscribers are correctly transferred/merged for users.
   *
   * @param array $user_1_subs
   *   User 1's initial subscriptions.
   * @param array $user_2_subs
   *   User 2's initial subscriptions.
   * @param array $expected_user_1_subs
   *   User 1's final subscriptions.
   * @param array $expected_user_2_subs
   *   User 2's final subscriptions.
   *
   * @dataProvider dataMerge
   * @covers ::performMerge
   */
  public function testMerge(array $user_1_subs, array $user_2_subs, array $expected_user_1_subs, array $expected_user_2_subs) {
    $this->user1 = $this->createDecoupledUser();
    $this->user2 = $this->createDecoupledUser();

    foreach ($user_1_subs as $sub_values) {
      $sub_values['uid'] = $this->user1->id();
      $subscriber = $this->subscriberStorage->create($sub_values);
      // With SimpleNews 2.x and newer, the mail value is prefilled from
      // the user so passing an explicit mail into storage->create
      // is immediately ovewritten with the user's mail.
      // For our tests, we want to always use the mail that's specified
      // in the test data, so we need a call to setMail after creation.
      $subscriber->setMail($sub_values['mail']);
      $subscriber->save();
    }

    foreach ($user_2_subs as $sub_values) {
      $sub_values['uid'] = $this->user2->id();
      $subscriber = $this->subscriberStorage->create($sub_values);
      // Must explicitly call setMail. See not above.
      $subscriber->setMail($sub_values['mail']);
      $subscriber->save();
    }

    $this->runMerge();

    $this->assertEquals($this->getSubscribers($this->user1), $expected_user_1_subs, 'User 1 subscribers');
    $this->assertEquals($this->getSubscribers($this->user2), $expected_user_2_subs, 'User 2 subscribers');
  }

  /**
   * Data provider for testing subscriber transfer/merges.
   */
  protected static function dataMerge() {
    // Only the primary has a subscriber.
    yield 'only-primary' => [
      'user_1_subs' => [
        [
          'id' => 1,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
      ],
      'user_2_subs' => [],
      'expected_user_1_subs' => [
        1 => TRUE,
      ],
      'expected_user_2_subs' => [],
    ];

    // Only the secondary has a subscriber.
    yield 'only-secondary' => [
      'user_1_subs' => [],
      'user_2_subs' => [
        [
          'id' => 1,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
      ],
      'expected_user_1_subs' => [
        1 => TRUE,
      ],
      'expected_user_2_subs' => [],
    ];

    // Both have a subscribers with the same email.
    yield 'both-single-match' => [
      'user_1_subs' => [
        [
          'id' => 1,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
      ],
      'user_2_subs' => [
        [
          'id' => 2,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
      ],
      'expected_user_1_subs' => [
        1 => TRUE,
      ],
      'expected_user_2_subs' => [
        2 => FALSE,
      ],
    ];

    // Both have a subscriber with different emails.
    yield 'both-single-different' => [
      'user_1_subs' => [
        [
          'id' => 1,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
      ],
      'user_2_subs' => [
        [
          'id' => 2,
          'mail' => 'test2@example.com',
          'status' => TRUE,
        ],
      ],
      'expected_user_1_subs' => [
        1 => TRUE,
        2 => TRUE,
      ],
      'expected_user_2_subs' => [],
    ];

    // Both have subscribers with one matching email.
    yield 'both-multiple-partial-match' => [
      'user_1_subs' => [
        [
          'id' => 1,
          'mail' => 'test1@example.com',
          'status' => TRUE,
        ],
        [
          'id' => 2,
          'mail' => 'test2@example.com',
          'status' => TRUE,
        ],
      ],
      'user_2_subs' => [
        [
          'id' => 3,
          'mail' => 'test2@example.com',
          'status' => TRUE,
        ],
        [
          'id' => 4,
          'mail' => 'test3@example.com',
          'status' => TRUE,
        ],
      ],
      'expected_user_1_subs' => [
        1 => TRUE,
        2 => TRUE,
        4 => TRUE,
      ],
      'expected_user_2_subs' => [
        3 => FALSE,
      ],
    ];
  }

  /**
   * Get the subscribers for a user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user we are checking for.
   *
   * @return array
   *   An array of subscriber IDs to statuses.
   */
  protected function getSubscribers(UserInterface $user) {
    $this->subscriberStorage->resetCache();
    /** @var \Drupal\simplenews\SubscriberInterface[] $subscriptions */
    $subscriptions = $this->subscriberStorage->loadByProperties(['uid' => $user->id()]);
    $comparison = [];
    foreach ($subscriptions as $subscription) {
      $id = (int) $subscription->id();
      $comparison[$id] = (bool) $subscription->getStatus();
    }
    ksort($comparison);
    return $comparison;
  }

}
