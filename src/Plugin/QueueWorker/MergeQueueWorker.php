<?php

namespace Drupal\entity_merge\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\entity_merge\Merger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Node Publisher that publishes nodes on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_entity_merge",
 *   title = @Translation("Cron Entity Merger"),
 *   cron = {"time" = 10}
 * )
 */
class MergeQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity merger.
   *
   * @var \Drupal\entity_merge\Merger
   */
  private $merger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_merge.merger'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Merger $merger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->merger = $merger;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $id = (int) $data;
    $this->merger->runById($id);
  }

}
