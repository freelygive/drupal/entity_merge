<?php

namespace Drupal\entity_merge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Context used for building the display of fields to merge.
 *
 * @package Drupal\entity_merge
 */
class FieldDisplayContext {

  /**
   * Fields to be displayed.
   *
   * @var array
   */
  private $fieldDisplayInfo = [];

  /**
   * Primary entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  private $primary;

  /**
   * Secondary entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  private $secondary;

  /**
   * Field ID.
   *
   * @var string
   */
  private $fieldId;

  /**
   * Primary field.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  private $primaryField;


  /**
   * Secondary field.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  private $secondaryField;

  /**
   * Parent merge context.
   *
   * @var \Drupal\entity_merge\MergeContext
   */
  private $parentContext;

  /**
   * FieldDisplayContext constructor.
   *
   * @param \Drupal\entity_merge\MergeContext $parent_context
   *   Parent context.
   * @param string $field_id
   *   Field ID.
   * @param \Drupal\Core\Field\FieldItemListInterface $primary_field
   *   Field list for primary entity.
   * @param \Drupal\Core\Field\FieldItemListInterface $secondary_field
   *   Field list for secondary entity.
   */
  public function __construct(MergeContext $parent_context, $field_id, FieldItemListInterface $primary_field, FieldItemListInterface $secondary_field) {
    $this->primary = $parent_context->getPrimary();
    $this->secondary = $parent_context->getSecondary();
    $this->fieldId = $field_id;
    $this->primaryField = $primary_field;
    $this->parentContext = $parent_context;
    $this->secondaryField = $secondary_field;
  }

  /**
   * Sets the field display definition.
   *
   * @param string $group
   *   The field group.
   * @param string $field_id
   *   The field ID to configure.
   * @param array $field_definition
   *   Array of field info with the following keys:
   *   - comparison: Whether the field values are equal or not
   *   - label: the field label text
   *   - primary: value from primary entity ready to render
   *   - secondary: value from secondary entity ready to render.
   */
  public function setFieldDisplayInfo($group, $field_id, array $field_definition) {
    $this->fieldDisplayInfo[$group][$field_id] = $field_definition;
  }

  /**
   * Gets the primary entity being merged.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getPrimary(): ContentEntityInterface {
    return $this->primary;
  }

  /**
   * Gets the secondary entity being merged.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getSecondary(): ContentEntityInterface {
    return $this->secondary;
  }

  /**
   * Gets the field ID.
   *
   * @return string
   *   The field ID.
   */
  public function getFieldId(): string {
    return $this->fieldId;
  }

  /**
   * Gets the field item list for this field from the primary entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list.
   */
  public function getPrimaryField(): FieldItemListInterface {
    return $this->primaryField;
  }

  /**
   * Gets the field item list for this field from the secondary entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list.
   */
  public function getSecondaryField(): FieldItemListInterface {
    return $this->secondaryField;
  }

  /**
   * Gets the parent merge context.
   *
   * @return \Drupal\entity_merge\MergeContext
   *   The parent merge context.
   */
  public function getParentContext(): MergeContext {
    return $this->parentContext;
  }

  /**
   * Gets the field value from the primary entity.
   *
   * @return array
   *   The field value.
   */
  public function getPrimaryValue() : array {
    return $this->primaryField->getValue();
  }

  /**
   * Gets the field value from the secondary entity.
   *
   * @return array
   *   The field value.
   */
  public function getSecondaryValue() : array {
    return $this->secondaryField->getValue();
  }

  /**
   * Gets the generated field display info.
   */
  public function getFieldDisplayInfo() {
    return $this->fieldDisplayInfo;
  }

}
