<?php

namespace Drupal\entity_merge;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Merge request entities.
 *
 * @ingroup entity_merge
 */
class MergeRequestListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['type'] = $this->t('Type');
    $header['primary'] = $this->t('Primary');
    $header['secondary'] = $this->t('Secondary');
    $header['status'] = $this->t('Status');
    $header['requested_by'] = $this->t('Requested By');
    $header['requested_at'] = $this->t('Requested At');
    $header['completed'] = $this->t('Completed At');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\entity_merge\Entity\MergeRequestInterface $entity */
    $primary = $entity->getPrimaryEntity();
    $secondary = $entity->getSecondaryEntity();
    $row['type'] = $entity->getTargetType();
    $row['primary'] = $this->getLink($primary);
    $row['secondary'] = $this->getLink($secondary);
    $row['status'] = $entity->getStatusLabel();
    $row['requested_by'] = $entity->getOwner()->toLink($entity->getOwner()
      ->label());
    $row['requested_at'] = date('Y-m-d H:i:s', $entity->getCreatedTime());
    $row['completed'] = $entity->completed->value ? date('Y-m-d H:i:s', $entity->completed->value) : '';
    return $row + parent::buildRow($entity);
  }

  /**
   * Get the link for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity to link to.
   *
   * @return \Drupal\Core\Link|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The generated link or text indicating the user doesn't exist.
   */
  protected function getLink(?EntityInterface $entity = NULL) {
    if ($entity == NULL) {
      return new TranslatableMarkup('[deleted user]');
    }

    $text = new FormattableMarkup('@label <small>[@id]</small>', [
      '@label' => $entity->label(),
      '@id' => $entity->id(),
    ]);
    if ($entity->getEntityTypeId() == 'user' && $this->moduleHandler->moduleExists('contacts')) {
      return Link::createFromRoute($text,
        'contacts.contact',
        ['user' => $entity->id()]);
    }
    else {
      return $entity->toLink($text);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $ops = [
      'view' => [
        'title' => $this->t('View'),
        'weight' => 1,
        'url' => Url::fromRoute('entity.entity_merge_request.canonical', ['entity_merge_request' => $entity->id()]),
      ],
    ];

    return $ops + parent::getDefaultOperations($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(FALSE)
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
