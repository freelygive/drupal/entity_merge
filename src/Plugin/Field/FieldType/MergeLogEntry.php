<?php

namespace Drupal\entity_merge\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Field type for merge request log entries.
 *
 * @FieldType(
 *   id = "entity_merge_log_entry",
 *   label = @Translation("Entity Merge Log Entry"),
 *   description = @Translation("Entity merge log entry"),
 *   category = @Translation("Entity Merge"),
 *   default_formatter = "entity_merge_log_entry"
 * )
 */
class MergeLogEntry extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['field'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Property'));

    $properties['primary_value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Primary entity value'));

    $properties['secondary_value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Secondary entity value'));

    $properties['message'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Message'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'field' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'primary_value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'secondary_value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'message' => [
          'type' => 'text',
        ],
      ],
    ];
  }

}
