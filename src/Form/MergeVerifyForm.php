<?php

namespace Drupal\entity_merge\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\entity_merge\Entity\MergeRequest;
use Drupal\entity_merge\Entity\MergeRequestInterface;
use Drupal\entity_merge\Merger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for verifying merge requests.
 *
 * @package Drupal\entity_merge\Form
 */
class MergeVerifyForm extends FormBase {

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Merge coordinator.
   *
   * @var \Drupal\entity_merge\Merger
   */
  protected $merger;

  /**
   * Queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * DedupForm constructor.
   */
  public function __construct(RendererInterface $renderer, DateFormatterInterface $date_formatter, Merger $merger, QueueFactory $queue_factory, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
    $this->merger = $merger;
    $this->queueFactory = $queue_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('date.formatter'),
      $container->get('entity_merge.merger'),
      $container->get('queue'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_merge_verify_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $primary_id = NULL, $secondary_id = NULL) {
    $entity_type = $this->getRouteMatch()->getParameter('entity_type');

    // Add our current selection (URL or form) for our merge submission.
    $form['primary_id'] = [
      '#type' => 'value',
      '#value' => $primary_id,
    ];
    $form['secondary_id'] = [
      '#type' => 'value',
      '#value' => $secondary_id,
    ];

    // If we're able to load our entities,
    // show a verification table with details.
    $this->buildMergeForm($form, $form_state, $entity_type, $primary_id, $secondary_id);

    if ($this->isMergeInProgress($primary_id, $secondary_id) && !$form_state->isRebuilding()) {
      $form['merge']['actions']['submit']['#disabled'] = TRUE;
      $this->messenger()->addWarning('A merge is already in progress for one of these records.');
    }
    elseif ($this->previouslyMerged($primary_id, $secondary_id) && !$form_state->isRebuilding()) {
      $this->messenger()->addWarning('One of these records has has already been merged.');
    }

    return $form;
  }

  /**
   * Builds the merge summary.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param string $entity_type
   *   Entity type id.
   * @param int $primary_id
   *   Primary entity id.
   * @param int $secondary_id
   *   Secondary entity id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildMergeForm(array &$form, FormStateInterface $form_state, $entity_type, $primary_id, $secondary_id) {
    $primary_entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($primary_id);
    $secondary_entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($secondary_id);

    if ($primary_entity && $secondary_entity) {
      $form['#attached']['library'][] = 'entity_merge/merge-form';

      $form['merge'] = [
        '#type' => 'container',
      ];

      $form['merge']['verify'] = [
        '#theme' => 'table',
        '#header' => [
          '',
          $this->getHeader($this->t('Primary'), $primary_entity),
          $this->getHeader($this->t('Secondary'), $secondary_entity),
        ],
        '#rows' => [],
      ];

      // Capture a flag so that custom callbacks can prevent merges.
      $field_groups = $this->merger->displayFields($entity_type, $primary_entity, $secondary_entity);

      foreach ($field_groups as $group => $properties) {
        if ($group) {
          $group_header = [
            [
              'colspan' => 3,
              'header' => TRUE,
              'data' => $group,
            ],
          ];
        }

        foreach ($properties as $property => $info) {
          $primary = $info['primary'];
          $secondary = $info['secondary'];

          // If we don't have either value we can skip out.
          if ($primary === NULL && $secondary === NULL) {
            continue;
          }

          // If the fields are field instances, render them.
          // Make sure entity references aren't generated as links.
          if ($primary instanceof FieldItemListInterface) {
            $primary = $primary->view([
              'label' => 'hidden',
              'settings' => ['link' => FALSE],
            ]);
            $primary = $this->renderer->render($primary);
          }
          if ($secondary instanceof FieldItemListInterface) {
            $secondary = $secondary->view([
              'label' => 'hidden',
              'settings' => ['link' => FALSE],
            ]);
            $secondary = $this->renderer->render($secondary);
          }

          // Output the group header if we have one.
          if (isset($group_header)) {
            $form['merge']['verify']['#rows'][$group] = $group_header;
            unset($group_header);
          }

          // Build our row.
          $row = [];
          $row['data'][0] = $info['label'];
          $row['data'][1] = $primary;
          $row['data'][2]['data'] = $secondary;

          if (isset($info['comparison'])) {
            $row['data'][2]['class'][] = 'merge-' . ($info['comparison'] ? 'match' : 'conflict');
          }

          // Add our row to the table.
          $form['merge']['verify']['#rows'][$property] = $row;
        }
      }

      $form['merge']['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => ['class' => ['messages', 'warning']],
        '#value' => $this->t('The colour coding provides an indication of what is likely to happen during the merge process. Results may vary according to specific circumstances.'),
      ];

      $form['merge']['actions'] = [
        '#type' => 'actions',
        '#weight' => 100,
      ];

      $form['merge']['actions']['submit'] = [
        '#type' => 'submit',
        '#name' => 'run_merge',
        '#value' => $this->t('Run merge'),
        '#attributes' => [
          'class' => ['button--primary'],
        ],
      ];

      $form['merge']['actions']['swap'] = [
        '#type' => 'link',
        '#title' => $this->t('Reverse'),
        '#url' => Url::fromRoute('entity.entity_merge_request.verify', [
          'entity_type' => $entity_type,
          'primary_id' => $secondary_id,
          'secondary_id' => $primary_id,
        ]),
        '#options' => [
          'attributes' => [
            'class' => [
              'button',
            ],
          ],
        ],
      ];
    }
  }

  /**
   * Build the header for an entity.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   The title of the header.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   The header.
   */
  protected function getHeader(TranslatableMarkup $title, EntityInterface $entity) {
    if ($entity->getEntityTypeId() == 'user' && $this->moduleHandler->moduleExists('contacts')) {
      $link = Link::createFromRoute($entity->label(),
        'contacts.contact',
        ['user' => $entity->id()]);
    }
    else {
      $link = $entity->toLink();
    }
    return new FormattableMarkup('<strong>@type</strong><br/><span>@label <small>[@id]</small></span>', [
      '@type' => $title,
      '@label' => $link->toString(),
      '@id' => $entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $primary_id = $form_state->getValue("primary_id");
    $secondary_id = $form_state->getValue("secondary_id");

    if ($this->isMergeInProgress($primary_id, $secondary_id)) {
      $this->messenger()->addWarning('A merge is already in progress for one of these records.');
      $form_state->setRebuild();
      return;
    }

    $this->messenger()->addStatus($this->t('Merge queued for processing.'));

    $entity_type = $this->getRouteMatch()->getParameter('entity_type');

    $request = MergeRequest::create([
      'primary_id' => $primary_id,
      'secondary_id' => $secondary_id,
      'target_type' => $entity_type,
    ]);

    $request->save();

    $queue = $this->queueFactory->get('cron_entity_merge');
    $queue->createItem($request->id());

    $form_state->setRedirect('entity.entity_merge_request.collection');
  }

  /**
   * Whether a merger is in progress.
   *
   * @param string $primary_id
   *   Primary entity id.
   * @param string $secondary_id
   *   Secondary entity id.
   *
   * @return bool
   *   Whether the merge is in progress.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function isMergeInProgress($primary_id, $secondary_id) : bool {
    // Check if a merge is already in progress for either of these users.
    $query = $this->entityTypeManager->getStorage('entity_merge_request')
      ->getQuery()
      ->accessCheck(FALSE);

    $group = $query->orConditionGroup()
      ->condition('primary_id', $primary_id)
      ->condition('primary_id', $secondary_id)
      ->condition('secondary_id', $primary_id)
      ->condition('secondary_id', $secondary_id);

    $match = $query->condition('status', [
      MergeRequestInterface::STATUS_FAILED,
      MergeRequestInterface::STATUS_SUCCESS,
    ], 'NOT IN')
      ->condition($group)
      ->count()
      ->execute();

    return $match > 0;
  }

  /**
   * Checks whether one of the records has previously been merged.
   *
   * @param string $primary_id
   *   Primary entity id.
   * @param string $secondary_id
   *   Secondary entity id.
   *
   * @return bool
   *   Whether one of the records has been merged.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function previouslyMerged($primary_id, $secondary_id) : bool {
    // Check if a merge is already in progress for either of these users.
    $query = $this->entityTypeManager->getStorage('entity_merge_request')
      ->getQuery()
      ->accessCheck(FALSE);

    $group = $query->orConditionGroup()
      ->condition('secondary_id', $primary_id)
      ->condition('secondary_id', $secondary_id);

    $match = $query->condition('status', MergeRequestInterface::STATUS_SUCCESS)
      ->condition($group)
      ->count()
      ->execute();

    return $match > 0;
  }

}
